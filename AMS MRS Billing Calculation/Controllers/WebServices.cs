﻿using AMS_MRS_Indisponibilités.helpers;
using Sita.AMSClient.AMSSoapService;
using Sita.AMSClient.MSMQNotifications;
using Sita.Logger;
using Sita.Toolkit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Reflection;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.ServiceModel.Configuration;
using System.ServiceModel.Description;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Web;
using System.Threading;

namespace AMS_MRS_Indisponibilités.Controllers
{
    class WebServices

    {


        public class EnableCrossOriginResourceSharingBehavior : BehaviorExtensionElement, IEndpointBehavior
        {
            public void AddBindingParameters(ServiceEndpoint endpoint, System.ServiceModel.Channels.BindingParameterCollection bindingParameters)
            {

            }

            public void ApplyClientBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.ClientRuntime clientRuntime)
            {

            }

            public void ApplyDispatchBehavior(ServiceEndpoint endpoint, System.ServiceModel.Dispatcher.EndpointDispatcher endpointDispatcher)
            {
                try
                {
                    var requiredHeaders = new Dictionary<string, string>();

                    requiredHeaders.Add("Access-Control-Allow-Origin", "*");
                    requiredHeaders.Add("Access-Control-Request-Method", "POST,GET,PUT,DELETE,OPTIONS");
                    requiredHeaders.Add("Access-Control-Allow-Headers", "X-Requested-With,Content-Type");

                    endpointDispatcher.DispatchRuntime.MessageInspectors.Add(new CustomHeaderMessageInspector(requiredHeaders));
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

            }

            public void Validate(ServiceEndpoint endpoint)
            {

            }

            public override Type BehaviorType
            {
                get { return typeof(EnableCrossOriginResourceSharingBehavior); }
            }

            protected override object CreateBehavior()
            {
                return new EnableCrossOriginResourceSharingBehavior();
            }
        }

        public class CustomHeaderMessageInspector : IDispatchMessageInspector
        {
            readonly Dictionary<string, string> requiredHeaders;
            public CustomHeaderMessageInspector(Dictionary<string, string> headers)
            {
                requiredHeaders = headers ?? new Dictionary<string, string>();
            }

            public object AfterReceiveRequest(ref System.ServiceModel.Channels.Message request, System.ServiceModel.IClientChannel channel, System.ServiceModel.InstanceContext instanceContext)
            {
                return null;
            }

            public void BeforeSendReply(ref System.ServiceModel.Channels.Message reply, object correlationState)
            {
                try
                {
                    var httpHeader = reply.Properties["httpResponse"] as HttpResponseMessageProperty;
                    foreach (var item in requiredHeaders)
                    {
                        httpHeader.Headers.Add(item.Key, item.Value);
                    }
                }
                catch (Exception ex)
                {
                    ex.ToString();
                }

            }
        }



        [ServiceContract]
        public interface IWebGui
        {
            [OperationContract]
            [WebGet(UriTemplate = "/{from}/{to}")]
            Stream do400Hzcalculation(string from, string to);


            [OperationContract]
            [WebGet(UriTemplate = "/6TCalculation/{from}/{to}")]
            Stream do6TCalculation(string from, string to);

            [OperationContract]
            [WebGet(UriTemplate = "status")]
            Stream getStatus();

            [OperationContract]
            [WebGet(UriTemplate = "progressStatus")]
            Stream getProgressStatus();

            [OperationContract]
            [WebGet(UriTemplate = "reset")]
            Stream resetCalculationProcess();

            [OperationContract]
            [WebGet(UriTemplate = "version")]
            Stream getVersion();

        }


        [ServiceBehavior(InstanceContextMode = InstanceContextMode.Single)]
        public class WebInterfaceFor400HzAndMinus6TCalculations : IWebGui
        {
            public bool isBusy { get; set; }

            Dictionary<string, Movement> myUniqueMovementDico = new Dictionary<string, Movement>(); // used for 400Hz calclaltions
            Dictionary<string, int[]> myAirlineToCountDico = new Dictionary<string, int[]>(); // int array will have 2 dimensions, 1st is arrival count, 2n is departure count
            DateTime fromDT = DateTime.MinValue;
            DateTime toDT = DateTime.MinValue;
            DateTime currentDateBeingProcessed = DateTime.MinValue;

            DateTime startTime = DateTime.MinValue;
            DateTime endTime = DateTime.MinValue;
            string detailOfLastPeriodReceived = "";

            public AirlineCollection myAirlinesInAMS { get; set; }
            List<Flight> myListOfFlightsToBeUpdatedAtTheEnd = new List<Flight>();
            int myNumberOfFlightsReallyUpdatedAtTheEnd = 0;


            List<Flight> myListOfFlightsThatAreNotreadyForConsolidation = new List<Flight>();
            int numberOfFlightsUpdated = 0;


            public Stream getVersion()
            {
                string toSendBack = "Version " + Assembly.GetExecutingAssembly().GetName().Version;
                MemoryStream ms1 = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(toSendBack));
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
                return ms1;
            }
            public Stream getStatus()
            {
                return do400Hzcalculation("", "");
            }

            public Stream resetCalculationProcess()
            {

                Guid guid = Guid.NewGuid();
                LogManager.TraceLog(Level.Info, "WebServices" //className
               , "Launch400Hz", guid.ToString(),
               "Received Reset Command ");

                if (AcquisitionbackgroundWorker.IsBusy == true)
                {
                    endTime = DateTime.MinValue;
                    isBusy = false;

                    AcquisitionbackgroundWorker.Abort();
                    AcquisitionbackgroundWorker.Dispose();
                    string toSendBack = "Relevé interrompu manuellement.";// status

                    Debug.WriteLine(toSendBack);

                    MemoryStream ms1 = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(toSendBack));
                    WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
                    return ms1;
                }
                else
                {
                    string toSendBack = "Le relevé n’est pas en cours...";// status



                    Debug.WriteLine(toSendBack);

                    MemoryStream ms1 = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(toSendBack));
                    WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
                    return ms1;
                }
            }

        
            public Stream getProgressStatus() // returns info for the progress bars
            {

                Guid guid = Guid.NewGuid();
                LogManager.TraceLog(Level.Info, "WebServices" //className
               , "getProgressSatatus", guid.ToString(),
               "Received getStatusProgress Command ");

                int numberOfDaysInRange = Convert.ToInt32((int)(toDTBracketinUTC -fromDTBracketinUTC).TotalDays  );
                int currentProgress = Convert.ToInt32((int)(currentDateBeingProcessed - fromDT).TotalDays) + 1 ;

                if (currentProgress < 0) currentProgress = 0;


                string toSendBack = (isBusy ? "true" : "false") + ";" // status
                    + fromDT.ToString("ddMMMy", CultureInfo.InvariantCulture) + "/" + toDT.ToString("ddMMMy", CultureInfo.InvariantCulture) + ";" //range of dates
                    + currentProgress.ToString() + "/" + numberOfDaysInRange.ToString() + ";" // current date
                    + myListOfFlightsToBeUpdatedAtTheEnd.Count.ToString() + "/" + myListOfFlightsThatAreNotreadyForConsolidation.Count.ToString() + ";" //status in the acquisition
                    + numberOfFlightsUpdated.ToString() + "/" + myListOfFlightsToBeUpdatedAtTheEnd.Count.ToString(); //status in the updates

           //     Debug.WriteLine(toSendBack);


                MemoryStream ms1 = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(toSendBack));
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
                return ms1;
            }

            int quinzaine = 0; // this is to determine if we want the first or second quinzaine.


            public Stream do6TCalculation(string from, string to)
            {
                return LaunchCalculationInThatMode("6Tons", from, to);

            }
            public Stream do400Hzcalculation(string from, string to)
            {
                return LaunchCalculationInThatMode("400Hz", from, to);
            
            }
            public Stream LaunchCalculationInThatMode(string mode400HzOr6T, string from, string to)
            {
                Guid guid = Guid.NewGuid();
                LogManager.TraceLog(Level.Info, "WebServices" //className
               , "LaunchCalculationInThatMode", guid.ToString(),
               "Received Calculation Command: " +mode400HzOr6T +" from/To: " + from + "/" + to);

                Console.WriteLine("Received Calculation Command: " + mode400HzOr6T + " from/To: " + from + "/" + to);
                if (isBusy) // an operation is in progress, do not take another one....
                {

                    string toSendBack = "Relevé en cours, commencé à "
                        + (startTime != DateTime.MinValue ? startTime.ToString("dd MMM yy HH:mm:ss.fff", CultureInfo.CreateSpecificCulture("fr-FR")) : "Never")
                        + "<br>" + "Date en cours d'analyse: " + currentDateBeingProcessed.ToString("dd MMM y", CultureInfo.CreateSpecificCulture("fr-FR"))
                       + "<br>" + "Résultat intermédiaire : relevé complet / Interrompu (Ok/not Ok): "
                        + myListOfFlightsToBeUpdatedAtTheEnd.Count.ToString() + "/" + myListOfFlightsThatAreNotreadyForConsolidation.Count.ToString()
                    + "<br>" + "Résultat final: relevé effectué / Total: "
               + numberOfFlightsUpdated.ToString() + "/" + myListOfFlightsToBeUpdatedAtTheEnd.Count.ToString(); //status in the updates

                    //      if (myListOfFlightsThatAreNotreadyFor400HzConsolidation.Count != 0) toSendBack = addFlightsWithIssues(toSendBack);
                    MemoryStream ms1 = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(toSendBack));
                    WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";

                    LogManager.TraceLog(Level.Info, "WebServices", "Launch400Hz", guid.ToString(), "do not perform calculation as we are busy");

                    return ms1;
                }


                else if (from.ToLower() == "check" && to.ToLower() == "only" || from.ToLower() == "")
                {
                  //  string toSendBack = "Version widget: " + Assembly.GetExecutingAssembly().GetName().Version + "<br>";
                    string toSendBack = "Version widget: " + Assembly.GetExecutingAssembly().GetName().Version + "<br>";

                    if (!allWentOKForPeriodCalculation)
                    {
                            toSendBack = "Erreur intervenue pendant le dernier relevé. <br>"
                            + errorStringTotal; // to signify there was a problem
                      
                    }

                    else
                    {

                        toSendBack += "Dernier relevé effectué: " + detailOfLastPeriodReceived
                      + "<br>Heure début: " + (startTime != DateTime.MinValue ? startTime.ToString("dd MMM yy HH:mm:ss.fff", CultureInfo.CreateSpecificCulture("fr-FR")) : "Aucun")
                      + "<br>Heure fin  : " + (endTime != DateTime.MinValue ? endTime.ToString("dd MMM yy HH:mm:ss.fff", CultureInfo.CreateSpecificCulture("fr-FR")) : "Aucun")
                      + "<br>" + "----- Résultat ----- "
                      + "<br>" + "Nombre total de vols pris en compte dans le calcul: " + myListOfFlightsToBeUpdatedAtTheEnd.Count.ToString()
                        + "<br>" + "Nombre total de vols effectivement mis à jour: " + myNumberOfFlightsReallyUpdatedAtTheEnd.ToString();

                        //if (myListOfFlightsToBeUpdatedAtTheEnd.Count != 0) // we no longer do this by company - DBP 10feb20
                        //{
                        //    toSendBack = toSendBack + "<br>" + "Par compagnie: ";
                        //    foreach (var item in myAirlineToCountDico)
                        //    {
                        //        if ((item.Value[0] + item.Value[1]) != 0)
                        //        {
                        //            toSendBack = toSendBack + "<br>" + "-- " + item.Key + ": " + (item.Value[0] + item.Value[1]).ToString();

                        //        }   
                        //    }
                        //}

                        if (myListOfFlightsThatAreNotreadyForConsolidation.Count != 0)
                        {

                            toSendBack = toSendBack + "<br>" + "Détails des " + myListOfFlightsThatAreNotreadyForConsolidation.Count.ToString() + " vols non traités: ";


                            toSendBack = addFlightsWithIssues(toSendBack);

                            toSendBack += errorStringTotal;
                        }
                    }



                    MemoryStream ms1 = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(toSendBack));
                    WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
                    return ms1;

                }


                // launch the hard core calculation
                if (mode400HzOr6T == "400Hz")
                {
                    return doThe400HzStuff(from, to, guid);
                }
                else
                {
                    return doTheMinus6TonsStuff(from, to, guid);
                }

               
            }

            public static Dictionary<Flight, List<Sita.AMSClient.AMSSoapService.PropertyValue>> myMinus6TFlightsToPVUpdateDico = new Dictionary<Flight, List<Sita.AMSClient.AMSSoapService.PropertyValue>>();


            private Stream doTheMinus6TonsStuff(string from, string to, Guid guid)
            {
                
                isBusy = true;
                Debug.WriteLine("isbusy true");

                LogManager.TraceLog(Level.Info, "WebServices" //className
                          , "doTheMinus6TonsStuff", guid.ToString(),
                          "Starting -6T Calculation");

                Console.WriteLine("Starting -6T Calculation");

                string output = "";

                myMinus6TFlightsToPVUpdateDico.Clear();
                myListOfFlightsToBeUpdatedAtTheEnd.Clear();
                myListOfFlightsThatAreNotreadyForConsolidation.Clear();

                try
                {
                    bool ok = DateTime.TryParseExact(from, "ddMMMyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fromDT);
                    if (!ok)
                    {
                        fromDT = DateTime.ParseExact(from, "dMMMyy", CultureInfo.InvariantCulture);
                    }

                    ok = DateTime.TryParseExact(to, "ddMMMyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out toDT);
                    if (!ok)
                    {
                        toDT = DateTime.ParseExact(to, "dMMMyy", CultureInfo.InvariantCulture);
                    }

                }
                catch (Exception)
                {

                    output = "Error parsing the date parameters ('" + from + "/" + to + "'). Correct format example: 01JUL19/15JUL19";
                }

                detailOfLastPeriodReceived = "-6 Tonnes: du " + from + " au " + to;

                if (output == "")
                {

                    output = "Démarrage du calcul des jours -6 tonnes sur la période";
                    startTime = DateTime.Now;
                    endTime = DateTime.MinValue;
                    numberOfFlightsUpdated = 0;
                    CalculateMinus6TonsOverAperiod(guid);   //<<<<<<< it is happening there!!
                }

                MemoryStream ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(output));
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
                return ms;

            }

            private void CalculateMinus6TonsOverAperiod(Guid guid)
            {
                errorStringTotal = "";

                AcquisitionbackgroundWorker = new AbortableBackgroundWorker();
                AcquisitionbackgroundWorker.WorkerReportsProgress = true;
                AcquisitionbackgroundWorker.DoWork += acquireTheMovementsAndWorkoutFlightsWithWithMinus6TonsInfo;
                AcquisitionbackgroundWorker.RunWorkerCompleted += worker_RunWorkerCompleted;

                AcquisitionbackgroundWorker.RunWorkerAsync(guid);

            }

            Dictionary<string, List<Movement>> myDicoOfMinus6TMovementBaséPerImmat = new Dictionary<string, List<Movement>>();

            string errorStringTotal = "";

            private void acquireTheMovementsAndWorkoutFlightsWithWithMinus6TonsInfo(object sender, DoWorkEventArgs e)
            {

                int HoursMarginFor6TPeriodAcquisition = Convert.ToInt32(CustomServiceManager.Configuration.widgetParameters["HoursMarginFor400HzPeriodAcquisition"]);


                Guid guid = (Guid)e.Argument;

                myUniqueMovementDico.Clear();
                myAirlineToCountDico.Clear();
                myDicoOfMinus6TMovementBaséPerImmat.Clear();
                myNumberOfFlightsReallyUpdatedAtTheEnd = 0;
                allWentOKForPeriodCalculation = true;
                fromDTBracketinUTC = TimeZoneInfo.ConvertTimeToUtc(fromDT.Date, CustomServiceManager.MRSTimeZone);
                toDTBracketinUTC = TimeZoneInfo.ConvertTimeToUtc(toDT.Date, CustomServiceManager.MRSTimeZone).AddHours(HoursMarginFor6TPeriodAcquisition);

           
              
                for (DateTime zeDay = fromDTBracketinUTC.AddHours(-HoursMarginFor6TPeriodAcquisition); zeDay <= toDTBracketinUTC; zeDay = zeDay.AddDays(1))
                {
                    currentDateBeingProcessed = zeDay;
                    Debug.WriteLine("Doing calculation for: " + zeDay.ToString("ddMMMyy", CultureInfo.InvariantCulture));

                    string errorStringForZeDay = "";

                   bool allWentOKForThisDayCalculation = !acquireAndWorkoutTheMinus6TForThisDay(guid, zeDay, out errorStringForZeDay);//<<<<<<< the data gathering is happening there!!

                    if (!allWentOKForThisDayCalculation)
                    {
                        allWentOKForPeriodCalculation = false;
                    }

                    if (!string.IsNullOrEmpty(errorStringForZeDay))
                    {
                        errorStringTotal += 
                            //"------ Erreurs le " + zeDay.ToString("ddMMMyy", CultureInfo.InvariantCulture) + "------<br>" + 
                            errorStringForZeDay;

                    }

                }


                bool SixTonsBasésAreOK  = Minus6TonsCalculation. ExamineTheListForMinus6TBasé(guid,myDicoOfMinus6TMovementBaséPerImmat, myMinus6TFlightsToPVUpdateDico); //<<<<<<< The real compute is happening there!!



                if (!allWentOKForPeriodCalculation)// || !SixTonsBasésAreOK) chnage on 25jan21
                    {
                        LogManager.TraceLog(Level.Control, "WebServices" //className
                   , "acquireTheMovementsAndUpdateFlightsWithWithMinus6TonsInfo", guid.ToString(),
                   "Error noted on minus 6 tons calculation");
                    ; // there has been a problem or an exception
                }
                //else chnage on 25jan21
                //{
                //and now send the updates to the flights over the period if necessary

                foreach (Flight myFlight in myMinus6TFlightsToPVUpdateDico.Keys)
                    {
                        FlightMethods.UpdateTheFlightWithTheseValuesIfNecessary(guid, myFlight, myMinus6TFlightsToPVUpdateDico[myFlight], out bool thereHasBeenARealUpdate);

                        if (thereHasBeenARealUpdate) myNumberOfFlightsReallyUpdatedAtTheEnd++;

                        numberOfFlightsUpdated++;
                    }

                //} chnage on 25jan21




                LogManager.TraceLog(Level.Info, "WebServices"  , "acquireTheMovementsAndUpdateFlightsWithWithMinus6TonsInfo", guid.ToString(), "Ending -6T Calculation");
            }

           

            private bool acquireAndWorkoutTheMinus6TForThisDay(Guid guid, DateTime zeDateTime, out string errorStringForTheDay)
            {
                 errorStringForTheDay = "";
                bool isThereAnyErrorDuringThatDay = false;

                // will store all movements for one day per non basée immat. In order to apply the basé rule (e.g.only one flight is billed)

                try
                {
                    ResultStatus result = CustomServiceManager.WidgetManager.AmsSoapClient.GetMovementsOverlappingWithPeriod(guid.ToString(), zeDateTime, zeDateTime.AddDays(1),
                        CustomServiceManager.Configuration.widgetParameters["HomeAirport"].ToString(),
                                  AirportIdentifierType.IATACode, out ErrorInformation[] error, out object data);



                    if (result == ResultStatus.Success)
                    {

                        LogManager.TraceLog(Level.Debug, "WebServices", "acquireAndUpdateTheMinus6TForThisDay", guid.ToString(),
               "Got flights for " + zeDateTime.ToString("ddMMyy", CultureInfo.InvariantCulture));

                        MovementCollection mMvtssInAMS = data as MovementCollection;

                        if (mMvtssInAMS != null && mMvtssInAMS.Movement != null)
                        {


                            int numberOfMovements = mMvtssInAMS.Movement.Length;
                            int indexOfMovement = 0;

                            foreach (Movement myMovement in mMvtssInAMS.Movement)
                            {
                                indexOfMovement++;

                                if (myMovement != null) // to sure we have a movement
                                {
                                    string key = getUniqueIdFor(myMovement);
                                    if (!myUniqueMovementDico.ContainsKey(key)) // to be sure it's not been done already
                                    {

                                        myUniqueMovementDico.Add(key, myMovement);


                                        string MTOWString = "";
                                        MTOWString = FlightMethods.getAttributeValueForMovement(myMovement, CustomServiceManager.Configuration.widgetParameters["PoidsMovementCF"].ToString());

                                        if (MTOWString != "")
                                        {
                                            try
                                            {
                                                int MTOW = Convert.ToInt32(MTOWString);
                                                if (Convert.ToInt32(MTOWString) < 6000)
                                                {

                                                    /// DO the work, but before check that we have a ggod movement to work from
                                                    /// 

                                                    bool thereIsAnErrorForThatMovement = false;

                                                    if (myMovement.ArrivalFlight == null)
                                                    {
                                                        errorStringForTheDay += flightRefFrom(myMovement.DepartureFlight) + ": Vol non lié " + "<br>";
                                                        thereIsAnErrorForThatMovement = true;
                                                    }
                                                    if (myMovement.DepartureFlight == null)
                                                    {
                                                        errorStringForTheDay += flightRefFrom(myMovement.ArrivalFlight) + ": Vol non lié " + "<br>";
                                                        thereIsAnErrorForThatMovement = true;
                                                    }
                                                    string myAIBTString = "";

                                                    if (myMovement.ArrivalFlight != null)
                                                    {
                                                         myAIBTString = FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, CustomServiceManager.Configuration.widgetParameters["AIBT"].ToString());
                                                        if (string.IsNullOrEmpty(myAIBTString))
                                                        {
                                                            errorStringForTheDay += flightRefFrom(myMovement.ArrivalFlight) + ": Pas d'heure bloc" + "<br>";
                                                            thereIsAnErrorForThatMovement = true;

                                                        }

                                                        if (FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, "DataLocked") == "true" && flightIsBetweenFromAndTo( myMovement.ArrivalFlight))
                                                        {
                                                            errorStringForTheDay += flightRefFrom(myMovement.ArrivalFlight) + ": Locked.<br>";
                                                            thereIsAnErrorForThatMovement = true;

                                                        }


                                                        string attArr = FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, CustomServiceManager.Configuration.widgetParameters["EtatFacturationCF_Arr"].ToString());

                                                        if (!CustomServiceManager.Configuration.widgetParameters["OnlyDoCalculationForFullMovementWithBothFlightsWithinFollowingStatus"].Contains(attArr)) // not the correct status for calculation on Arr
                                                        {
                                                            errorStringForTheDay += flightRefFrom(myMovement.ArrivalFlight) + ": Pas le bon état facturation '" + attArr + "'<br>";
                                                            thereIsAnErrorForThatMovement = true;
                                                        }

                                                    }

                                                    string myAOBTString = "";

                                                    if (myMovement.DepartureFlight!= null)
                                                    {

                                                         myAOBTString = FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, CustomServiceManager.Configuration.widgetParameters["AOBT"].ToString());
                                                        if (string.IsNullOrEmpty(myAOBTString))
                                                        {
                                                            errorStringForTheDay += flightRefFrom(myMovement.DepartureFlight) + ": Pas d'heure bloc" + "<br>";
                                                            thereIsAnErrorForThatMovement = true;
                                                        }

                                                        string attDep = FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, CustomServiceManager.Configuration.widgetParameters["EtatFacturationCF_Dep"].ToString());
                                                        if (!CustomServiceManager.Configuration.widgetParameters["OnlyDoCalculationForFullMovementWithBothFlightsWithinFollowingStatus"].Contains(attDep)) // not the correct status for calculation on Arr
                                                        {
                                                            errorStringForTheDay += flightRefFrom(myMovement.DepartureFlight) + ": Pas le bon état facturation '" + attDep + "'<br>";
                                                            thereIsAnErrorForThatMovement = true;
                                                        }
                                                        

                                                        if (FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, "DataLocked") == "true" &&  flightIsBetweenFromAndTo(myMovement.DepartureFlight))
                                                        {
                                                            errorStringForTheDay += flightRefFrom(myMovement.DepartureFlight) + ": Locked.<br>";
                                                            thereIsAnErrorForThatMovement = true;

                                                        }

                                                    }


                                                   





                                                    if (!thereIsAnErrorForThatMovement)
                                                    {

                                                        DateTime AIBT = DateTime.ParseExact(myAIBTString, "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                                                        DateTime AOBT = DateTime.ParseExact(myAOBTString, "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                                                        if ((AOBT < fromDT) || AIBT > toDT.AddHours(23).AddMinutes(59))
                                                        {
                                                            continue; // this departed before the from or landed after the todate. No need to worry 
                                                        }


                                                        LogManager.TraceLog(Level.Debug, "WebServices", "acquireAndUpdateTheMinus6TForThisDay", guid.ToString(), "Entering -6T");

                                                        string AvionBaseString = FlightMethods.getAttributeValueForMovement(myMovement, CustomServiceManager.Configuration.widgetParameters["AvionBaseMovementCF"].ToString());


                                                        if (AvionBaseString.ToLower() == "oui")
                                                        {
                                                            addTheMovementToTheDicoIfOK(myDicoOfMinus6TMovementBaséPerImmat, myMovement);

                                                         
                                                        }
                                                        else
                                                        {
                                                            if (myMovement.ArrivalFlight!= null && flightIsBetweenFromAndTo(myMovement.ArrivalFlight))
                                                            {
                                                                Minus6TonsCalculation.doTheDurationCalculationForLessThan6TonsNonBase(guid, myMovement, myMinus6TFlightsToPVUpdateDico);
                                                            }
                                                        }
                                                        myListOfFlightsToBeUpdatedAtTheEnd.Add(myMovement.ArrivalFlight);
                                                    }
                                                    else
                                                    {
                                                        myListOfFlightsThatAreNotreadyForConsolidation.Add(myMovement.ArrivalFlight);
                                                        isThereAnyErrorDuringThatDay = true;
                                                    }
                                                }
                                            }
                                            catch (Exception)
                                            {
                                                string MvtRef = buildMvtRefFor(myMovement);

                                                LogManager.TraceLog(Level.Error, "WebServices", "acquireAndUpdateTheMinus6TForThisDay", guid.ToString(), MvtRef + " MTOW conversion pb. Cannot convert to number the value: " + MTOWString);
                                                errorStringForTheDay += MvtRef + ": MTOW problème de conversion. Valeur: " + MTOWString + "<br>";
                                                isThereAnyErrorDuringThatDay = true;
                                            }


                                        }
                                        else
                                        {
                                            errorStringForTheDay += flightRefFrom(myMovement.ArrivalFlight) + "/" + flightRefFrom(myMovement.DepartureFlight) + ": Pas de MTOW" + "<br>";
                                            isThereAnyErrorDuringThatDay = true;
                                        }
                                    }

                                }

                            }
                        }
                    }
                    else
                    {
                        LogManager.TraceLog(Level.Error, "WebServices",
                            "acquireAndUpdateTheMinus6TForThisDay", guid.ToString(),
                            "Problem getting movements for " + zeDateTime.ToString("ddMMyy", CultureInfo.InvariantCulture));
                        isThereAnyErrorDuringThatDay = true;
                       

                    }

                }


                catch (Exception ee)
                {


                    LogManager.TraceLog(Level.Error, "WebServices",
                                        "acquireAndUpdateTheMinus6TForThisDay", guid.ToString(),
                                        "Exception when processing flights for " + zeDateTime.ToString("ddMMyy", CultureInfo.InvariantCulture) + " -- " + ee.Message);


                    isThereAnyErrorDuringThatDay = true;
                }

                return isThereAnyErrorDuringThatDay;
            }

            private bool flightIsBetweenFromAndTo(Flight myFlight)
            {
                if (myFlight.FlightId.ScheduledDate >= fromDT.Date && myFlight.FlightId.ScheduledDate <= toDT.Date)
                {
                    return true;
                }
                return false;
            }

            private bool itIsTheRightDay(DateTime zeDateTime, string myAIBTString)
            {
                DateTime AIBT = DateTime.ParseExact(myAIBTString, "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                return (AIBT.Date == zeDateTime);
            }

            private void addTheMovementToTheDicoIfOK(Dictionary<string, List<Movement>> myDicoOfBaseImmatForZeDay, Movement myMovement)
            {
                // we are sure that there is an immat as we know the MTOW and we know it is non basé

            

               string  immat = myMovement.ArrivalFlight.FlightState.Aircraft.AircraftId.Registration;

                if (!myDicoOfBaseImmatForZeDay.ContainsKey(immat))
                {
                    myDicoOfBaseImmatForZeDay.Add(immat, new List<Movement>());
                }

                myDicoOfBaseImmatForZeDay[immat].Add(myMovement);
                
            }

            private string buildMvtRefFor(Movement myMovement)
            {

                Sita.AMSClient.MSMQNotifications.Flight arrflight = myMovement.ArrivalFlight;
                Sita.AMSClient.MSMQNotifications.Flight depflight = myMovement.DepartureFlight;

                return flightRefFrom(arrflight) + "/" + flightRefFrom(depflight);

            }

            private string flightRefFrom(Sita.AMSClient.MSMQNotifications.Flight flight)
            {
                if (flight == null) return "";
              
                Sita.AMSClient.MSMQNotifications.FlightId flightId = flight.FlightId;

               return (flightId.FlightKind== Sita.AMSClient.MSMQNotifications.FlightKind.Arrival? "Arr ":"Dep ")+ flightId.AirlineDesignator[0].Value + flightId.FlightNumber 
                    + " du " + flightId.ScheduledDate.ToString("ddMMMyy",CultureInfo.InvariantCulture)
                    + " (" + flight.FlightState.Aircraft.AircraftId.Registration +")";
            }

            private Stream doThe400HzStuff(string from, string to, Guid guid)
            {
                isBusy = true;
                Debug.WriteLine("isbusy true");

                LogManager.TraceLog(Level.Info, "WebServices" //className
                          , "doThe400HzStuff", guid.ToString(),
                          "Starting 400Hz Calculation");

                string output = "";

                myListOfFlightsToBeUpdatedAtTheEnd.Clear();
                myListOfFlightsThatAreNotreadyForConsolidation.Clear();



                try
                {
                    bool ok = DateTime.TryParseExact(from, "ddMMMyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out fromDT);
                    if (!ok)
                    {
                        fromDT = DateTime.ParseExact(from, "dMMMyy", CultureInfo.InvariantCulture);
                    }

                    ok = DateTime.TryParseExact(to, "ddMMMyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out toDT);
                    if (!ok)
                    {
                        toDT = DateTime.ParseExact(to, "dMMMyy", CultureInfo.InvariantCulture);
                    }

                }
                catch (Exception)
                {

                    output = "Error parsing the date parameters ('" + from + "/" + to + "'). Correct format example: 01JUL19/15JUL19";
                }

                if (from.Substring(0, 2) == "16")
                {
                    quinzaine = 2;
                    fromDT = fromDT.AddDays(-15); // we need to acquire the entire month to include GA calculation.

                }
                else
                {
                    quinzaine = 1;
                }




                if (output == "")
                {



                    try
                    {
                        DateTime.TryParseExact(from, "dMMMyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime myFromDT);

                        if (myFromDT.Date.Day == 1)
                        {
                            detailOfLastPeriodReceived = "400Hz - 1ère quinzaine de ";

                        }
                        else
                        {
                            detailOfLastPeriodReceived = "400Hz - 2ème quinzaine de ";

                        }



                        DateTime month = myFromDT.Date.AddDays(myFromDT.Day - 1);
                        detailOfLastPeriodReceived = detailOfLastPeriodReceived + month.ToString("y", CultureInfo.CreateSpecificCulture("fr-FR"));
                    }
                    catch (Exception)
                    {

                    }

                    output = "Démarrage du relevé des prises 400Hz sur la " + detailOfLastPeriodReceived;
                    startTime = DateTime.Now;
                    endTime = DateTime.MinValue;
                    numberOfFlightsUpdated = 0;
                    consolidate400HzOverAPeriod(guid);
                }

                MemoryStream ms = new MemoryStream(System.Text.Encoding.UTF8.GetBytes(output));
                WebOperationContext.Current.OutgoingResponse.ContentType = "text/html";
                return ms;
            }

            private string addFlightsWithIssues(string toSendBack)
            {
                {
                    toSendBack = toSendBack + "<br>" + "Vols non mis à jour car ils n'ont pas le bon status: " + "<br>";

                    int maxIssuesToDisplay = Convert.ToInt32(CustomServiceManager.Configuration.widgetParameters["MaxNumberOfFlightsIssuesReported"].ToString());

                    foreach (Flight item in myListOfFlightsThatAreNotreadyForConsolidation)
                    {

                        string currentStatus = "";

                        if (item.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival)
                        {
                            currentStatus = FlightMethods.getCFValueForFlight(item, CustomServiceManager.Configuration.widgetParameters["EtatFacturationCF_Arr"].ToString());

                        }
                        else
                        {
                            currentStatus = FlightMethods.getCFValueForFlight(item, CustomServiceManager.Configuration.widgetParameters["EtatFacturationCF_Dep"].ToString());

                        }

                        toSendBack = toSendBack + "<br>"
                            + item.FlightId.FlightKind.ToString().Substring(0,3) + " " + item.FlightId.AirlineDesignator[0].Value.ToString() 
                            + item.FlightId.FlightNumber + TimeZoneInfo.ConvertTimeFromUtc(item.FlightState.ScheduledTime, CustomServiceManager.MRSTimeZone).ToString(" le ddMMMyy HH:mm ", CultureInfo.CreateSpecificCulture("fr-FR"))  
                            + "(H. Loc.) avec état \""
                           + currentStatus + "\"";

                        if (myListOfFlightsThatAreNotreadyForConsolidation.IndexOf(item) >= maxIssuesToDisplay)
                        {

                            toSendBack = toSendBack + "<br> .... Liste tronquées après " + maxIssuesToDisplay.ToString() + " vols sur un total de " 
                                + myListOfFlightsThatAreNotreadyForConsolidation.Count.ToString() + " ...";
                            break;
                        }
                                
                    }
                }

                return toSendBack;
            }

            AbortableBackgroundWorker AcquisitionbackgroundWorker;

            private void consolidate400HzOverAPeriod(Guid guid)
            {

                 AcquisitionbackgroundWorker = new AbortableBackgroundWorker();
                AcquisitionbackgroundWorker.WorkerReportsProgress = true;
                AcquisitionbackgroundWorker.DoWork += acquireTheMovementsAndUpdateAirlinesWith400HzInfo;
                AcquisitionbackgroundWorker.RunWorkerCompleted += worker_RunWorkerCompleted;
             
                AcquisitionbackgroundWorker.RunWorkerAsync(guid);

            }

            public class AbortableBackgroundWorker : BackgroundWorker
            {

                private Thread workerThread;

                protected override void OnDoWork(DoWorkEventArgs e)
                {
                    workerThread = Thread.CurrentThread;
                    try
                    {
                        base.OnDoWork(e);
                    }
                    catch (ThreadAbortException)
                    {
                        e.Cancel = true; //We must set Cancel property to true!
                        Thread.ResetAbort(); //Prevents ThreadAbortException propagation
                    }
                }


                public void Abort()
                {
                    if (workerThread != null)
                    {
                        workerThread.Abort();
                        workerThread = null;
                    }
                }
            }

            DateTime fromDTBracketinUTC = DateTime.MinValue; // stores the boundaries
            DateTime toDTBracketinUTC = DateTime.MinValue;

            bool allWentOKForPeriodCalculation = true;


            private void acquireTheMovementsAndUpdateAirlinesWith400HzInfo(object sender, DoWorkEventArgs e)
            {

                int HoursMarginFor400HzPeriodAcquisition = Convert.ToInt32(CustomServiceManager.Configuration.widgetParameters["HoursMarginFor400HzPeriodAcquisition"]);


                Guid guid = (Guid)e.Argument;

                myUniqueMovementDico.Clear();
                myAirlineToCountDico.Clear();
                myNumberOfFlightsReallyUpdatedAtTheEnd = 0;

                fromDTBracketinUTC = TimeZoneInfo.ConvertTimeToUtc(fromDT.Date, CustomServiceManager.MRSTimeZone);
                toDTBracketinUTC = TimeZoneInfo.ConvertTimeToUtc(toDT.Date, CustomServiceManager.MRSTimeZone).AddHours(HoursMarginFor400HzPeriodAcquisition);

                allWentOKForPeriodCalculation = true;

                for (DateTime zeDay = fromDTBracketinUTC.AddHours(-HoursMarginFor400HzPeriodAcquisition); zeDay <= toDTBracketinUTC; zeDay = zeDay.AddDays(1))
                {
                    currentDateBeingProcessed = zeDay;
                    Debug.WriteLine("Doing calculation for: " + zeDay.ToString("ddMMMyy", CultureInfo.InvariantCulture));
                    allWentOKForPeriodCalculation = acquireAndUpdateThe400HzForThisDay(guid, zeDay);

                    if (!allWentOKForPeriodCalculation)
                    {
                        LogManager.TraceLog(Level.Error, "WebServices" //className
                   , "Launch400Hz", guid.ToString(),
                   "Stopping acquisition over the period");
                        break; // there has been a problem or an exception
                    }
                }

                //and now send the updates to the flights over the period if necessary

                if (allWentOKForPeriodCalculation)

                {
                    foreach (Flight myFlight in myListOfFlightsToBeUpdatedAtTheEnd)
                    {
                        string airline = myFlight.FlightId.AirlineDesignator[0].Value.ToString();
                        UpdateIfNecessaryThatFlightWithThatCountOf400Hz(guid, myFlight, myAirlineToCountDico[airline]);
                        numberOfFlightsUpdated++;
                    }


                }
                LogManager.TraceLog(Level.Info, "WebServices" //className
           , "Launch400Hz", guid.ToString(),
           "Ending 400Hz Calculation");
            }



            private bool acquireAndUpdateThe400HzForThisDay(Guid guid, DateTime zeDateTime)
            {

            
                try
                {
                    ResultStatus result = CustomServiceManager.WidgetManager.AmsSoapClient.GetMovementsOverlappingWithPeriod(guid.ToString(), zeDateTime, zeDateTime.AddDays(1),
                        CustomServiceManager.Configuration.widgetParameters["HomeAirport"].ToString(),
                                  AirportIdentifierType.IATACode, out ErrorInformation[] error, out object data);



                    if (result == ResultStatus.Success)
                    {

                        LogManager.TraceLog(Level.Debug, "WebServices" //className
               , "acquireAndUpdateThe400HzForThisDay", guid.ToString(),
               "Got flights for " + zeDateTime.ToString("ddMMyy", CultureInfo.InvariantCulture));

                        MovementCollection mMvtssInAMS = data as MovementCollection;

                        if (mMvtssInAMS != null && mMvtssInAMS.Movement != null)
                        {
                           

                            int numberOfMovements = mMvtssInAMS.Movement.Length;
                            int indexOfMovement = 0;

                            foreach (Movement myMovement in mMvtssInAMS.Movement)
                            {
                                indexOfMovement++;


                                if (myMovement != null) // to sure we have a movement
                                {

                                    string key = getUniqueIdFor(myMovement);
                                    if (!myUniqueMovementDico.ContainsKey(key)) // to be sure it's not been done already
                                    {
                                        if (key == "MVT_134406")
                                        {

                                        }

                                        myUniqueMovementDico.Add(key, myMovement);

                                        /// DO the work
                                        /// 


                                        int[] count = getThe400HzCountFor(myMovement, zeDateTime);


                                  // create and update the flight count

                                        if (myMovement.ArrivalFlight != null)
                                        {
                                            string arrAirlineThatShouldBeCounted = myMovement.ArrivalFlight.FlightId.AirlineDesignator[0].Value;
                                            if (!myAirlineToCountDico.ContainsKey(arrAirlineThatShouldBeCounted)) myAirlineToCountDico.Add(arrAirlineThatShouldBeCounted, new int[] { 0, 0 });
                                            myAirlineToCountDico[arrAirlineThatShouldBeCounted][0] = myAirlineToCountDico[arrAirlineThatShouldBeCounted][0] + count[0];

                                        }

                                        if (myMovement.DepartureFlight != null)
                                        {
                                            string depAirlineThatShouldBeCounted = myMovement.DepartureFlight.FlightId.AirlineDesignator[0].Value;
                                            if (!myAirlineToCountDico.ContainsKey(depAirlineThatShouldBeCounted)) myAirlineToCountDico.Add(depAirlineThatShouldBeCounted, new int[] { 0, 0 });
                                            myAirlineToCountDico[depAirlineThatShouldBeCounted][1] = myAirlineToCountDico[depAirlineThatShouldBeCounted][1] + count[1];

                                        }





                                    }
                                }
                               
                            }
                        }
                    }
                    else
                    {
                        LogManager.TraceLog(Level.Error, "WebServices", 
                            "acquireAndUpdateThe400HzForThisDay", guid.ToString(), 
                            "Problem getting flights for " + zeDateTime.ToString("ddMMyy", CultureInfo.InvariantCulture));


                        return false;

                    }

                }


                catch (Exception ee)
                {


                    LogManager.TraceLog(Level.Error, "WebServices",
                                        "acquireAndUpdateThe400HzForThisDay", guid.ToString(),
                                        "Exception when processing flights for " + zeDateTime.ToString("ddMMyy", CultureInfo.InvariantCulture) + " -- " + ee.Message);


                    return false;
                }

                return true;
            }


            private string getUniqueIdFor(Object myInput)
            {
                if (myInput.GetType() == typeof(Flight))
                {
                    Flight myFlight = (Flight)myInput;


                    foreach (Sita.AMSClient.MSMQNotifications.PropertyValue PV in myFlight.FlightState.Value)
                    {
                        if (PV.propertyName == "FlightUniqueID")
                        {
                            return PV.Value.ToString();
                        }
                    }
                }
                if (myInput.GetType() == typeof(Movement))
                {
                    Movement myMovement = (Movement)myInput;


                    foreach (Sita.AMSClient.MSMQNotifications.PropertyValue PV in myMovement.MovementState.Value)
                    {
                        if (PV.propertyName == "MovementUniqueID")
                        {
                            return PV.Value.ToString();
                        }
                    }
                }


                return "";


            }

            // to check with Maher !!!!!!!!
            private int[] getThe400HzCountFor(Movement myMovement, DateTime zeDateTime)
            {

                int countToReturnArr = 0;
                int countToReturnDep = 0;

                string CFForArrPeriodicty = CustomServiceManager.Configuration.widgetParameters["Arr_BillingPeriodicity"];
                string CFForDepPeriodicty = CustomServiceManager.Configuration.widgetParameters["Dep_BillingPeriodicity"];

                string Periodicity_CFValue_for15J = CustomServiceManager.Configuration.widgetParameters["Periodicity_CFValue_for15J"];
                string Periodicity_CFValue_for30J = CustomServiceManager.Configuration.widgetParameters["Periodicity_CFValue_for30J"];
                string EtatOKpourCalculPrises400Hz = CustomServiceManager.Configuration.widgetParameters["EtatOKpourCalculPrises400Hz"].ToString()   ;

               string EtatExclupourVerificationCalculPrises400Hz = CustomServiceManager.Configuration.widgetParameters["EtatExclupourVerificationCalculPrises400Hz"].ToString() +
                    CustomServiceManager.Configuration.widgetParameters["EtatFacturationAfterConsolidated400Hz"].ToString();// we must contune to count the flights that have been calculated already, hence the concantenation



                if (myMovement.ArrivalFlight != null) // arrival part of the movement
                {
                    if (myMovement.ArrivalFlight.FlightId.FlightNumber == "1362")
                    {

                    }
                   

                    string currentArrValue = FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, CustomServiceManager.Configuration.widgetParameters["NombrePrise400HzCF_Arr"].ToString()).ToString();

                    if (currentArrValue == "") currentArrValue = "0";

                    DateTime myAIBT = DateTime.MinValue;

                    Debug.WriteLine("Doing " + myMovement.ArrivalFlight.FlightId.ScheduledDate.ToShortDateString()
                        + myMovement.ArrivalFlight.FlightId.FlightKind.ToString()
                       + myMovement.ArrivalFlight.FlightId.AirlineDesignator[0].Value
                        + myMovement.ArrivalFlight.FlightId.FlightNumber);

                    string currentArrStatus = FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, CustomServiceManager.Configuration.widgetParameters["EtatFacturationCF_Arr"].ToString());
                    string myAIBTString = FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, CustomServiceManager.Configuration.widgetParameters["AIBT"].ToString());
               
                    if (myMovement.ArrivalFlight.FlightId.FlightNumber == "930"  )
                    {

                    }

                    if (myAIBTString != "")
                    {
                        myAIBT = DateTime.ParseExact(myAIBTString, "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);



                        if (myAIBT >= fromDT && myAIBT < toDT.AddDays(1))
                        {
                            string periodicityInFlight = FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, CFForArrPeriodicty);

                            bool isAnOK15JPeriodicityFlight = false; // will check all the criterias for this
                            bool isAnOK30JPeriodicityFlight = false;


                            if (periodicityInFlight == Periodicity_CFValue_for30J && quinzaine == 2) isAnOK30JPeriodicityFlight = true;// vérification que le CF periodicité est OK
                            if (periodicityInFlight == Periodicity_CFValue_for15J) isAnOK15JPeriodicityFlight = true;// vérification que le CF periodicité est OK

                            if (quinzaine == 2)
                            {
                                if (myAIBT < fromDT.AddDays(15)) isAnOK15JPeriodicityFlight = false; // to be sure we don't count for 15J the flights that are in the 1st qunzaine


                            }


                            if (isAnOK15JPeriodicityFlight || isAnOK30JPeriodicityFlight)
                            {
                              
                                if (EtatOKpourCalculPrises400Hz.Contains(currentArrStatus)) // we do the calculation only if the status is correct, 
                                {
                                    myListOfFlightsToBeUpdatedAtTheEnd.Add(myMovement.ArrivalFlight); // we will update all flights even if they don't have the correct count.

                                    countToReturnArr = Convert.ToInt32(currentArrValue);
                                }
                                else // otheriwse, we put it in the list of failed flights.

                                {
                                    if (CustomServiceManager.Configuration.widgetParameters["EtatFacturationAfterConsolidated400Hz"].ToString() != currentArrStatus)
                                    {
                                        if (!EtatExclupourVerificationCalculPrises400Hz.Contains( currentArrStatus))
                                        {
                                            myListOfFlightsThatAreNotreadyForConsolidation.Add(myMovement.ArrivalFlight); // add to the list of bad flights only if it is a flight with wrong status, not if the flight has already been calculated
                                        }

                                    }
                                  
                                }
                            }
                        }
                    }
                   

                }
                if (myMovement.DepartureFlight != null)
                // for the departure part
                {
                    string currentDepValue = FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, CustomServiceManager.Configuration.widgetParameters["NombrePrise400HzCF_Dep"].ToString());
                    if (currentDepValue == "") currentDepValue = "0";

                    DateTime myAOBT = DateTime.MinValue;
                    if (myMovement.DepartureFlight.FlightId.AirlineDesignator[0].Value
                        + myMovement.DepartureFlight.FlightId.FlightNumber == "V72496")
                    {

                    }

                    Debug.WriteLine("Doing " + myMovement.DepartureFlight.FlightId.ScheduledDate.ToShortDateString()
                        + myMovement.DepartureFlight.FlightId.FlightKind.ToString()
                       + myMovement.DepartureFlight.FlightId.AirlineDesignator[0].Value
                        + myMovement.DepartureFlight.FlightId.FlightNumber);

                    string currentDepStatus = FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, CustomServiceManager.Configuration.widgetParameters["EtatFacturationCF_Dep"].ToString());
                    string myAOBTString = FlightMethods.getCFValueForFlight(myMovement.DepartureFlight,   CustomServiceManager.Configuration.widgetParameters["AOBT"].ToString());

                    if (myAOBTString != "")
                    {

                        myAOBT = DateTime.ParseExact(myAOBTString, "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                        if (myAOBT >= fromDT && myAOBT < toDT.AddDays(1))
                        {
                            string periodicityInFlight = FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, CFForDepPeriodicty);

                            bool isAnOK15JPeriodicityFlight = false; // will check all the criterias for this
                            bool isAnOK30JPeriodicityFlight = false;


                            if (periodicityInFlight == Periodicity_CFValue_for30J && quinzaine == 2) isAnOK30JPeriodicityFlight = true;// vérification que le CF periodicité est OK
                            if (periodicityInFlight == Periodicity_CFValue_for15J) isAnOK15JPeriodicityFlight = true;// vérification que le CF periodicité est OK

                            if (quinzaine == 2)
                            {
                                if (myAOBT < fromDT.AddDays(15)) isAnOK15JPeriodicityFlight = false; // to be sure we don't count for 15J the flights that are in the 1st qunzaine


                            }
                            if (isAnOK15JPeriodicityFlight || isAnOK30JPeriodicityFlight)
                            {
                               
                                if (EtatOKpourCalculPrises400Hz.Contains(currentDepStatus))
                                {
                                    myListOfFlightsToBeUpdatedAtTheEnd.Add(myMovement.DepartureFlight); // we will update all flights even if they don't have the correct count.

                                    countToReturnDep = Convert.ToInt32(currentDepValue);
                                }
                                else

                                {

                                    if (CustomServiceManager.Configuration.widgetParameters["EtatFacturationAfterConsolidated400Hz"].ToString() != currentDepStatus)
                                    {
                                        if (!EtatExclupourVerificationCalculPrises400Hz.Contains(currentDepStatus))
                                        {
                                            myListOfFlightsThatAreNotreadyForConsolidation.Add(myMovement.DepartureFlight); // add to the list of bad flights only if it is a flight with wrong status, not if the flight has already been calculated
                                        }
                                    }
                                }
                            }
                        }
                    }

                    else
                    {// we do not report if th AIBT does not exist - 11 feb20
                        //if (myMovement.DepartureFlight.FlightState.ScheduledTime >= fromDT400HzinUTC && myMovement.DepartureFlight.FlightState.ScheduledTime < toDT400HzinUTC)
                        //{
                        //    myListOfFlightsThatAreNotreadyFor400HzConsolidation.Add(myMovement.DepartureFlight);
                        //}
                    }

                }

                return new int[] { countToReturnArr, countToReturnDep };
            }


            private void UpdateIfNecessaryThatFlightWithThatCountOf400Hz(Guid guid, Flight myFlight, int[] v)
            {
                try
                {
                    string targetValue = (v[0] + v[1]).ToString();

                    if (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival)

                    // below does the update only if the count needs to be changed - 11Feb20

                    {
                        if (FlightMethods.getCFValueForFlight(myFlight, CustomServiceManager.Configuration.widgetParameters["historicalCountFor400HzAirlineCF_Arr"].ToString()) != targetValue
                            || FlightMethods.getCFValueForFlight(myFlight, CustomServiceManager.Configuration.widgetParameters["EtatFacturationCF_Arr"]) != CustomServiceManager.Configuration.widgetParameters["EtatFacturationAfterConsolidated400Hz"].ToString())
                        {

                            myNumberOfFlightsReallyUpdatedAtTheEnd++;

                            FlightMethods.UpdateTheFlightWithTheseValuesIfNecessary(guid, myFlight, new List<Sita.AMSClient.AMSSoapService.PropertyValue>
                                    {
                                        new Sita.AMSClient.AMSSoapService.PropertyValue
                                        {
                                            propertyNameField =CustomServiceManager.Configuration.widgetParameters["historicalCountFor400HzAirlineCF_Arr"].ToString(),
                                            valueField = targetValue
                                        },

                                        new Sita.AMSClient.AMSSoapService.PropertyValue
                                        {
                                            propertyNameField =CustomServiceManager.Configuration.widgetParameters["TimeLastUpdate400HzAirlineCF_Arr"].ToString(),
                                            valueField =DateTime.Now.ToString()
                                        },

                                        new Sita.AMSClient.AMSSoapService.PropertyValue
                                        {
                                            propertyNameField =CustomServiceManager.Configuration.widgetParameters["EtatFacturationCF_Arr"].ToString(),
                                            valueField =CustomServiceManager.Configuration.widgetParameters["EtatFacturationAfterConsolidated400Hz"].ToString()
                                        }


                                    }, out bool thereHasBeenARealUpdate
                            );
                        }

                    }

                    else
                    {
                        if (FlightMethods.getCFValueForFlight(myFlight, CustomServiceManager.Configuration.widgetParameters["historicalCountFor400HzAirlineCF_Dep"].ToString()) != targetValue
                            || FlightMethods.getCFValueForFlight(myFlight, CustomServiceManager.Configuration.widgetParameters["EtatFacturationCF_Dep"]) != CustomServiceManager.Configuration.widgetParameters["EtatFacturationAfterConsolidated400Hz"].ToString())
                        {
                            myNumberOfFlightsReallyUpdatedAtTheEnd++;


                            FlightMethods.UpdateTheFlightWithTheseValuesIfNecessary(guid, myFlight, new List<Sita.AMSClient.AMSSoapService.PropertyValue>
                                    {
                                        new Sita.AMSClient.AMSSoapService.PropertyValue
                                        {
                                            propertyNameField =CustomServiceManager.Configuration.widgetParameters["historicalCountFor400HzAirlineCF_Dep"].ToString(),
                                            valueField = targetValue
                                        },

                                        new Sita.AMSClient.AMSSoapService.PropertyValue
                                        {
                                            propertyNameField =CustomServiceManager.Configuration.widgetParameters["TimeLastUpdate400HzAirlineCF_Dep"].ToString(),
                                            valueField =DateTime.Now.ToString()
                                        },

                                        new Sita.AMSClient.AMSSoapService.PropertyValue
                                        {
                                            propertyNameField =CustomServiceManager.Configuration.widgetParameters["EtatFacturationCF_Dep"].ToString(),
                                            valueField =CustomServiceManager.Configuration.widgetParameters["EtatFacturationAfterConsolidated400Hz"].ToString()
                                        }



                                    }, out bool thereHasBeenARealUpdate
                        );
                        }
                    }

                }
                catch (Exception)
                {
                    LogManager.TraceLog(Level.Error, "WebServices" //className
                          , "UpdateThatFlightWithThatCountOf400Hz", guid.ToString(),
                          "Error Updating Flight: " + (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival ? "Arr" : "Dep")
                          + myFlight.FlightId.AirlineDesignator[0].Value + myFlight.FlightId.FlightNumber
                         + " " + myFlight.FlightId.ScheduledDate.ToString("ddMMMyy", CultureInfo.InvariantCulture)); ;
                }





            }





            private string getICAOCodeForAirline(string airlineString)
            {
                foreach (AirlineInformation myAirlineInfo in myAirlinesInAMS.Airline)
                {
                    if (myAirlineInfo.AirlineId[0].Value == airlineString)
                    {
                        return myAirlineInfo.AirlineId[1].Value;

                    }
                }

                return "";

            }



            private void worker_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
            {

                endTime = DateTime.Now;
                isBusy = false;
                Debug.WriteLine("isbusy FALSE");



            }
        }

    }
}
