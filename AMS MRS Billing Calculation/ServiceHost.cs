﻿using Sita.Configuration;
using Sita.Configuration.Common;
using Sita.Logger;
using Sita.Toolkit;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.ServiceProcess;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace AMS_MRS_Indisponibilités
{
    public partial class ServiceHost : ServiceBase
    {
        #region Attributes
        /// <summary>
        /// The service manager instance
        /// </summary>
        CustomServiceManager ServiceManager = null;
        /// <summary>
        /// The widget configuration instance
        /// </summary>
        public WidgetConfiguration Configuration { get; set; } = new WidgetConfiguration();
        /// <summary>
        /// The widget manager instance that handles all MSMQ filtering and AMS authentication
        /// </summary>
        public WidgetManager WidgetManager { get; set; } = null;
        #endregion

        #region Constructor
        public ServiceHost()
        {
            InitializeComponent();
        }
        #endregion

        #region Methods

        protected override void OnStart(string[] args)
        {
            AppDomain.CurrentDomain.UnhandledException += new UnhandledExceptionEventHandler(OnExceptionUnhandledException);

            Guid guid = Guid.NewGuid();
            string TransactionId = guid.ToString();
            Task.Factory.StartNew(Initialize).ContinueWith(c =>
            {
                AggregateException exception = c.Exception;

                OnTaskException(exception, TransactionId);
            });

        }

        /// <summary>
        /// Initializes the widget manager and the configuration
        ///
        public void Initialize()
        {



          //  Thread.Sleep(10000); // for debugging when attaching the project to a service 

            //Initialize the widget manager
            InitializeWidgetManager();

            //Subscribe to message received event
            WidgetManager.MessageReceivedEvent += new WidgetManager.WantedMessageReceived(MessageReceived);


            try
            {
                string waitMinutesForRestartInfailover = Configuration.widgetParameters["WaitingforAmsThread"];

                if (waitMinutesForRestartInfailover != "")
                {
                    Thread.Sleep(1000 * 60 * Convert.ToInt32(waitMinutesForRestartInfailover));
                }
            }
            catch (Exception)
            {
                
            }
           

            //Initialize the widget manager
            WidgetManager.Initialize();

            //Instantiate the service manager
            ServiceManager = new CustomServiceManager(this);

            //Read Configuration Variables
            ServiceManager.ReadConfigurationVariables();

            ServiceManager.InitializeWidgetFunctionalMetrics();


        }

        /// <summary>
        /// Start the worker on the Custom Manager
        /// </summary>
    
        /// <summary>
        /// This function serializes the configuration file path and instantiates the widget manager
        /// </summary>
        private void InitializeWidgetManager()
        {
            //Set the current directory to the windows service directory
            string exeDir = Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
            Directory.SetCurrentDirectory(exeDir);

            //Get the configuration file path from settings file
            string path = Properties.Settings.Default.ConfigurationFile;

            //Deserialize the configuration object
            Configuration = Utils.XMLDeserializeAndEncryptPasswords(Configuration, path) as WidgetConfiguration;

            if (Configuration != null)
            {
                //Instantiate the widget manager
                WidgetManager = new WidgetManager(Configuration);
            }

        }


        /// <summary>
        /// This function is subscribes to the message event received
        /// whenever a message that applies to all changes is received, it will be invoked
        /// </summary>
        /// <param name="message">The message content</param>
        /// <param name="queueName">The queue name that holds that message</param>
        /// <param name="returnedObject">The returned object that contains all information</param>
        public void MessageReceived(string message, string queueName, string serverName, string messageType, object returnedObject, string TransactionId)
        {
            //Run the task that will treat the message
            Task.Factory.StartNew(() => ServiceManager.TaskTreatMessage(message, queueName, serverName, messageType, returnedObject, TransactionId)).ContinueWith(c =>
             {
                 AggregateException exception = c.Exception;

                 OnTaskException(exception, TransactionId);
             });
        }

        protected override void OnStop()
        {
            Guid guid = Guid.NewGuid();
            string TransactionId = guid.ToString();

            //stop threads or dispose resources from service manager
            if (ServiceManager != null)
                ServiceManager.OnStopService(TransactionId);

            //check if the widget manager is not null and stop it
            if (WidgetManager != null)
                WidgetManager.Stop(TransactionId);
        }

        private void OnTaskException(AggregateException aggregateException, string TransactionId)
        {
            if (aggregateException != null)
            {
               // WidgetManager.DiagnosticsManager.IncrementMessagesTreatedWithError();

                //Iterate on the AggregateException and log errors
                foreach (var ex in aggregateException.InnerExceptions)
                    LogManager.TraceLog(Level.Error, "ServiceHost", ex.TargetSite.Name, TransactionId, ex.Message);
            }
        }
        /// <summary>
        /// The function that will be invoked whenever an exception is not cought
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="args"></param>
        public void OnExceptionUnhandledException(object sender, UnhandledExceptionEventArgs args)
        {
            //Create the transaction Id
            Guid guid = Guid.NewGuid();
            string transactionId = guid.ToString();

            //Cast the exception
            Exception ex = args.ExceptionObject as Exception;

            //Check if the exception is null or not
            if (ex != null)
                LogManager.TraceLog(Level.Error, "ServiceHost", ex.TargetSite.Name, transactionId, ex.Message);
        }
        #endregion
    }
}
