﻿using Sita.Configuration;
using Sita.Toolkit;
using Sita.Logger;
using System;
using System.Collections.Generic;
using System.IO;
using System.Globalization;
using Sita.AMSClient.MSMQNotifications;
using System.ServiceModel;
using System.ServiceModel.Web;
using System.Xml.Serialization;
using static AMS_MRS_Indisponibilités.Controllers.WebServices;
using  AMS_MRS_Indisponibilités.helpers;
using Sita.AMSClient.AMSSoapService;
using System.Diagnostics;
using Newtonsoft.Json;
using System.Net.Configuration;

namespace AMS_MRS_Indisponibilités
{
    // to do, auto update when changing or adding stands.
    // optimsation consolidation of the updates done by various parts of widget 
    // web service securty, veriy that we have a call back from AMS, ie it si done through extension

    public class    CustomServiceManager
    {
        #region Attributes
        /// <summary>
        /// The widget configuration instance
        /// </summary>
        public static WidgetConfiguration Configuration { get; set; }
        /// <summary>
        /// The widget manager instance that handles all MSMQ filtering and AMS authentication
        /// </summary>
        public static WidgetManager WidgetManager { get; set; } = null;


        //private FunctionalMetrics mvtMessages { get; set; }
        //private FunctionalMetrics flightMessages { get; set; }

        #endregion

        #region Constuctor
        public CustomServiceManager(ServiceHost serviceHost)
        {
            Configuration = serviceHost.Configuration;
            WidgetManager = serviceHost.WidgetManager;
        }
        #endregion

       static  Dictionary<string, string> myStandDico = new Dictionary<string, string>();


        public static string getParameterByDate(string settingName)
        {
            return getParameterByDate(settingName, DateTime.Now);
        }


        public static string getMRSParameterByDate(string settingName, DateTime myDateToCheckInUTC)
        {


            if (!CustomServiceManager.Configuration.widgetParameters.ContainsKey(settingName))
            {
                LogManager.TraceLog(Level.Error, "TimeVariableSetting", "", "getMRSParameterByDate", "The Setting does not exist in config.xml: " + settingName);

                return "";
            }
                

            string valuesToReturn = CustomServiceManager.Configuration.widgetParameters[settingName];

            string toreturn = "";

            try
            {
                string[] valuesbyTime = valuesToReturn.Split('>');

               toreturn = valuesbyTime[0];


                for (int i = 1; i < valuesbyTime.Length; i++)
                {
                    string myDateString = valuesbyTime[i].Split(':')[0];

                    if (myDateToCheckInUTC >= DateTime.ParseExact(myDateString, "ddMMMyy", CultureInfo.InvariantCulture))
                    {
                        return valuesbyTime[i].Split(':')[1];

                    }


                }
            }
            catch (Exception)
            {
                LogManager.TraceLog(Level.Debug, "TimeVariableSetting", "", "getParameterByDate", "Error trying to deserialize a value that looks like a dated parameter: " + valuesToReturn);

            }

            return toreturn;

        }

        public static string getParameterByDate(string settingName, DateTime myDateToCheck)
        {
            if (!CustomServiceManager.Configuration.widgetParameters.ContainsKey(settingName))
            {
                LogManager.TraceLog(Level.Error, "TimeVariableSetting", "", "getParameterByDate", "The Setting does not exist in config.xml: " + settingName);

                return "";
            }
            string valueToReturn = CustomServiceManager.Configuration.widgetParameters[settingName];

            if (settingName.StartsWith("{") && settingName.EndsWith("}")) // there may be a correct object to deserialze
            {
                try
                {
                    var settings = new JsonSerializerSettings { DateFormatString = "ddMMMyy" };
                    var myObject = JsonConvert.DeserializeObject(valueToReturn, settings);

                    if (myObject.GetType() ==  typeof(TimeVariableSetting[]))
                    {
                        TimeVariableSetting[] mySettingsArray =(TimeVariableSetting[])myObject;

                        foreach (var item in mySettingsArray)
                        {
                            if (myDateToCheck > item.StartDate)
                            {
                                return item.Value;
                            }

                        }
                    } 

                }
                catch (Exception)
                {
                    LogManager.TraceLog(Level.Debug, "TimeVariableSetting", "", "getParameterByDate", "Error trying to deserialize a value that looks like a dated parameter: " + valueToReturn);

                }

            }

            return valueToReturn;

        }




        public void InitializeWidgetFunctionalMetrics()
        {

           
            //mvtMessages = new FunctionalMetrics ("1", "Number of MOVEMENT Messages Received");
            //WidgetManager.DiagnosticsManager.AddFunctionalMetrics(mvtMessages);
            //flightMessages = new FunctionalMetrics ("2", "Number of FLIGHT Messages Received" );
            //WidgetManager.DiagnosticsManager.AddFunctionalMetrics(flightMessages);

        }


        public static TimeZoneInfo MRSTimeZone  = TimeZoneInfo.FindSystemTimeZoneById("Romance Standard Time");

        public WebServiceHost host = null;
        #region Methods
        public void ReadConfigurationVariables()
        {


            // Read widget specific configuration variables
            // Read the worker delay from the configuration file 
            string addressForWebService = "";
            try
            {

                

                WebInterfaceFor400HzAndMinus6TCalculations webGui = new WebInterfaceFor400HzAndMinus6TCalculations(); // initialise the webSevices

                //WebServiceHost host = new WebServiceHost(webGui, new Uri("http://localhost:" + Configuration.widgetParameters["WebServicePort"] + Configuration.widgetParameters["WebServiceEndPoint"]));
                 addressForWebService = "http://" +  Configuration.widgetParameters["WebServiceBaseURL"] + ":" + Configuration.widgetParameters["WebServicePort"] + "/" + Configuration.widgetParameters["WebServiceEndPoint"];

                 host = new WebServiceHost(webGui, new Uri(addressForWebService));
                var bindings = new WebHttpBinding();
                bindings.CrossDomainScriptAccessEnabled = true;
                //var serviceEndpoint = host.AddServiceEndpoint(typeof(IWebGui), bindings, "http://localhost:" + Configuration.widgetParameters["WebServicePort"] + Configuration.widgetParameters["WebServiceEndPoint"]);
                var serviceEndpoint = host.AddServiceEndpoint(typeof(IWebGui), bindings, addressForWebService);
                serviceEndpoint.Behaviors.Add(new EnableCrossOriginResourceSharingBehavior());
                host.Open();
                LogManager.TraceLog(Level.Control, "CustomServiceManager", "", "ReadConfigurationVariables", "OK Init Web Service Controllers at: " + addressForWebService);


                Console.WriteLine("OK Init Web Service Controllers at: " + addressForWebService);

            }
            catch (Exception ex)
            {
                LogManager.TraceLog(Level.Error, "CustomServiceManager", "", "ReadConfigurationVariables", "Error initialising Web Service Controllers at: " + addressForWebService + ". Reason given: " + ex.Message + " -- " + ex.InnerException);
                Console.WriteLine("Error initialising Web Service Controllers at: " + addressForWebService + ". Reason given: " + ex.Message + " -- " + ex.InnerException);

            }



            FlightMethods.initStandDico(myStandDico);

            
            if (myStandDico.Count != 0)
            {
                LogManager.TraceLog(Level.Info, "CustomServiceManager", "", "ReadConfigurationVariables", "Started OK");
                Console.WriteLine(myStandDico.Count + " stands Acquired from REST API. Started OK!");

            }
            else
            {
                LogManager.TraceLog(Level.Error, "CustomServiceManager", "", "ReadConfigurationVariables", "No Stand Acquired from REST API. Please check!");
                Console.WriteLine("No Stand Acquired from REST API. Please check!");
            }


            IndispoManagement.InitTheIndispoEnvironment(); // initialize the Indispo jobs,.

        }



        /// <summary>
        /// The task that will treat the message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="queueName"></param>
        /// <param name="serverName"></param>
        /// <param name="returnedObject"></param>
        /// <param name="transactionId"></param>
        public void TaskTreatMessage(string message, string queueName, string serverName, string messageType, object returnedObject, string transactionId)
        {
            // Implement Message treatements
            // Increment messages treated with succes number at the end

            Guid guid = Guid.NewGuid();

            string originUser = "";

            try
            {
                XmlSerializer serializer = new XmlSerializer(typeof(Sita.AMSClient.MSMQNotifications.Envelope));

                Sita.AMSClient.MSMQNotifications.Envelope myEnvelope = null;
                using (StringReader reader = new StringReader(message))
                {
                    myEnvelope = (Sita.AMSClient.MSMQNotifications.Envelope)serializer.Deserialize(reader);
                    reader.Close();
                }

                originUser = myEnvelope.user;

                if (myEnvelope.user.Replace(" ", string.Empty) == Configuration.amsConfiguration.amsSoapConfiguration.authenticationConfiguration.username.Replace(" ", string.Empty))
                {
                    return; // Do nothing as this is an echo of what we have modified from the widgetw
                }  

            }
            catch (Exception)
            {
            }

            switch (messageType)
            {
                

                case "MovementUpdatedNotification":
                    //Process Flight Updated Notification
                    MovementUpdatedNotification movementUpdatedNotification = returnedObject as MovementUpdatedNotification;

                    try
                    {
                        string Arrival = movementUpdatedNotification.Movement.ArrivalFlight.FlightId.AirlineDesignator[0].Value + movementUpdatedNotification.Movement.ArrivalFlight.FlightId.FlightNumber;
                        string Departure = movementUpdatedNotification.Movement.DepartureFlight.FlightId.AirlineDesignator[0].Value + movementUpdatedNotification.Movement.DepartureFlight.FlightId.FlightNumber;

                        LogManager.TraceLog(Level.Debug, "CustomServiceManager", "TaskTreatMessage", guid.ToString(),"Origin: " + originUser + "!!! Starting a new entry for " + Arrival + "/" + Departure + " "+ DateTime.Now.ToString("ss.fff", CultureInfo.InvariantCulture));


                        Console.WriteLine("Starting a new entry for " + Arrival + "/" + Departure);
                    }
                    catch (Exception ex)
                    {
                        LogManager.TraceLog(Level.Error, "CustomServiceManager", "TaskTreatMessage", guid.ToString(), "Exception: " + ex.Message);

                    }
                    ProcessMovementUpdatedNotification(guid, movementUpdatedNotification.Movement);
                    break;

                case "StandDowngradeCreatedNotification":
                    StandDowngradeCreatedNotification standDowngradeCreatedNotification = returnedObject as StandDowngradeCreatedNotification;
                 IndispoManagement.TreatStandDowngradeCreatedNotification(guid, standDowngradeCreatedNotification);
                    break;
                case "StandDowngradeUpdatedNotification":
                    StandDowngradeUpdatedNotification standDowngradeUpdatedNotification = returnedObject as StandDowngradeUpdatedNotification;
                    IndispoManagement.TreatStandDowngradeUpdatedNotification(guid, standDowngradeUpdatedNotification);
                    break;
                case "StandDowngradeDeletedNotification":
                    StandDowngradeDeletedNotification standDowngradeDeletedNotification = returnedObject as StandDowngradeDeletedNotification;
                    IndispoManagement.TreatStandDowngradeDeletedNotification(guid, standDowngradeDeletedNotification);
                    break;

                default:
                    return;
            }

        }



        private void ProcessMovementUpdatedNotification(Guid guid, Movement myMovement)
        {
            FlightMethods.myRealTimeFlightsToPVUpdateDico.Clear();

            try
            {
                bool movementOKWithDurationCalculation = myMovementIsOKWithDurationCalculation(myMovement);
                bool movementOKWithIndispoAndStatNuitCalculation = myMovementIsOKPourCalculIndispoEtStatNuit(myMovement);
                if (!movementOKWithDurationCalculation)
                {
                    Console.WriteLine("NOT for duration calculation");

                    LogManager.TraceLog(Level.Debug, "CustomServiceManager", "ProcessMovementUpdatedNotification", guid.ToString(), "Received Movement that Should not be calculated for duration");
                }
                if (!movementOKWithIndispoAndStatNuitCalculation)
                {
                    Console.WriteLine("NOT for indispo and stat de nuit.");

                    LogManager.TraceLog(Level.Debug, "CustomServiceManager", "ProcessMovementUpdatedNotification", guid.ToString(), "Received Movement that Should not be calculated for Indispo and stat de Nuit");
                }
                initializeThePVListWithAllResetValue(guid, myMovement); // allows to reset all CF's that are not overwritten by the calculations, to be sure in case of changes where the calculation conditions will not longer apply

                if (movementOKWithIndispoAndStatNuitCalculation)
                {
                    LogManager.TraceLog(Level.Debug, "CustomServiceManager", "ProcessMovementUpdatedNotification", guid.ToString(), "Entering Indispo job");
                    IndispoManagement.TreatMovementUpdatedNotificationForIndispo(guid, myMovement);
                }

                if (movementOKWithIndispoAndStatNuitCalculation)
                {
                    LogManager.TraceLog(Level.Debug, "CustomServiceManager", "ProcessMovementUpdatedNotification", guid.ToString(), "Entering Passerelle job");
                    doThePasserelleDurationJob(guid, myMovement);
                }

                if (movementOKWithIndispoAndStatNuitCalculation)
                {
                    LogManager.TraceLog(Level.Debug, "CustomServiceManager", "ProcessMovementUpdatedNotification", guid.ToString(), "Out of Passerellejob, entering stationnement de nuit");
                    stationnementDeNuitCFValueMemory = "";
                    doTheStationnementDeNuitJob(guid, myMovement);
                }


                if (movementOKWithDurationCalculation)
                {
                    LogManager.TraceLog(Level.Debug, "CustomServiceManager", "ProcessMovementUpdatedNotification", guid.ToString(), "Out of stationnement de nuit");
                    doTheDurationCalculationForAllMovements(guid, myMovement);

                   

                    LogManager.TraceLog(Level.Debug, "CustomServiceManager", "ProcessMovementUpdatedNotification", guid.ToString(), "Out of calculation, entering abris");
                    doTheAbrisCalculation(guid, myMovement); // should be done on all movements



                    LogManager.TraceLog(Level.Debug, "CustomServiceManager", "ProcessMovementUpdatedNotification", guid.ToString(), "Out of Abris.");
                }



                /// now is the time to send all updates
                /// 

                FlightMethods.DoTheConsolidatedUpdateOnFlights(guid);
                LogManager.TraceLog(Level.Debug, "CustomServiceManager", "ProcessMovementUpdatedNotification", guid.ToString(), "Done consolidated update. Finished");



                if (WidgetManager.DiagnosticsManager!= null)
                {

                    WidgetManager.DiagnosticsManager.IncrementMessagesTreatedWithSuccess();
                    LogManager.TraceLog(Level.Control, "CustomServiceManager", "TaskTreatMessage", guid.ToString(), "Incremented Mvt count");
                }


            }
            catch (Exception EX)
            {
                LogManager.TraceLog(Level.Error, "CustomServiceManager", "TreatMovementUpdatedNotification", guid.ToString(), "Processed movement Updated Not OK, Exception: " + EX.Message);
                if (WidgetManager.DiagnosticsManager != null)
                {
                    WidgetManager.DiagnosticsManager.IncrementMessagesTreatedWithError();
                }
            }

        }

        private bool myMovementIsOKWithDurationCalculation(Movement myMovement)
        {
            if (myMovement.ArrivalFlight == null || myMovement.DepartureFlight == null || 
                !thatFlightHasBlockTime(myMovement.ArrivalFlight) || !thatFlightHasBlockTime(myMovement.DepartureFlight)) return false;


            string attArr = FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, Configuration.widgetParameters["EtatFacturationCF_Arr"].ToString());
            string attDep = FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, Configuration.widgetParameters["EtatFacturationCF_Dep"].ToString());



            if (! Configuration.widgetParameters["OnlyDoCalculationForFullMovementWithBothFlightsWithinFollowingStatus"].Contains(attArr)
               || ! Configuration.widgetParameters["OnlyDoCalculationForFullMovementWithBothFlightsWithinFollowingStatus"].Contains(attDep))
              
            {
                return false;
            }

            return true;
           
        }



        private bool myMovementIsOKPourCalculIndispoEtStatNuit(Movement myMovement)
        {
            bool arrNotExistsOrNoBlock = myMovement.ArrivalFlight == null || !thatFlightHasBlockTime(myMovement.ArrivalFlight);
            bool depNotExistsOrNoBlock = myMovement.DepartureFlight == null || !thatFlightHasBlockTime(myMovement.DepartureFlight);

            if ( arrNotExistsOrNoBlock && depNotExistsOrNoBlock) return false;


            bool arrCorrectStatus = false;
            bool depCorrectStatus = false;


            if (myMovement.ArrivalFlight != null)
            {
                string attArr = FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, Configuration.widgetParameters["EtatFacturationCF_Arr"].ToString());
                if (Configuration.widgetParameters["EtatOKPourCalculForIndispoEtStatNuit"].Contains(attArr))
                {
                    arrCorrectStatus = true;
                }
            }


            if (myMovement.DepartureFlight!= null)
            {
                string attDep = FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, Configuration.widgetParameters["EtatFacturationCF_Dep"].ToString());
                if (Configuration.widgetParameters["EtatOKPourCalculForIndispoEtStatNuit"].Contains(attDep))

                {
                    depCorrectStatus= false;
                }
            }

            if (!arrCorrectStatus && !depCorrectStatus) return false;
         
               

            return true;

        }

        private void doTheAbrisCalculation(Guid guid, Movement myMovement)
        {
            // do only if correct Status for billing

           

            if (myMovement.ArrivalFlight != null && myMovement.DepartureFlight != null
                && FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, Configuration.widgetParameters["AIBT"].ToString()) != ""
                && FlightMethods.getCFValueForFlight(myMovement.DepartureFlight,Configuration.widgetParameters["AOBT"].ToString()) != "")

            {


                // just the count of days that have a least a slot in Abris.


                DateTime myAIBT = DateTime.ParseExact(FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, Configuration.widgetParameters["AIBT"].ToString()), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                DateTime myAOBT = DateTime.ParseExact(FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, Configuration.widgetParameters["AOBT"].ToString()), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                List<StandSlot> standSlots = new List<StandSlot>(); // this will be the list that we will work on
                standSlots.AddRange(myMovement.ArrivalFlight.FlightState.StandSlots);


                List<StandSlot> slotsWithinFranchise = new List<StandSlot>();


                ApplyTheFranchiseToTheseStandSlots(myAIBT, standSlots, out slotsWithinFranchise); // shave the list with the franchise, useless here ??? 

                int count = 0;

                for (DateTime zeDayINMRSTIme = TimeZoneInfo.ConvertTimeFromUtc(myAIBT, MRSTimeZone).Date; zeDayINMRSTIme <= TimeZoneInfo.ConvertTimeFromUtc(myAOBT, MRSTimeZone).Date; zeDayINMRSTIme = zeDayINMRSTIme.AddDays(1)) // check all the days,
                {
                    if (ThereIsAnAbrisSlotInThatDay(standSlots, zeDayINMRSTIme)) count++;

                }


                Console.WriteLine("Avant: " + myMovement.ArrivalFlight.FlightId.AirlineDesignator[0].Value + ":" + count.ToString());


                // check if the plane pays a Forfait (-6T non basé). In that case reduce the count by 1 (change 9oct19)

                string AvionBaseString = FlightMethods.getAttributeValueForMovement(myMovement, Configuration.widgetParameters["AvionBaseMovementCF"].ToString());

               string  MTOWString = FlightMethods.getAttributeValueForMovement(myMovement, Configuration.widgetParameters["PoidsMovementCF"].ToString());

                //if (MTOWString != "") // rechnage on 16 oct
                //{
                //    // There is a forfait only for the -6T: 16 oct

                //    if (Convert.ToInt32(MTOWString) <6000 && count >= 1) count--;

                //}

            


                if (MTOWString != "" && AvionBaseString != "Oui" && Convert.ToInt32(MTOWString) < 6000)
                {
                    if (count >= 1) count--;

                }

                // send the value to the update consolidation method

                List<Sita.AMSClient.AMSSoapService.PropertyValue> PVToSend = new List<Sita.AMSClient.AMSSoapService.PropertyValue>();
                PVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                {
                    propertyNameField = Configuration.widgetParameters["StatJourAbris"].ToString(),
                    valueField = count.ToString()

                });



                Debug.WriteLine("Après: " + myMovement.ArrivalFlight.FlightId.AirlineDesignator[0].Value + ":" + count.ToString());

                FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, myMovement.ArrivalFlight, PVToSend);

            }
        }

     

   
      
      

   
        private bool ThereIsAnAbrisSlotInThatDay(List<StandSlot> standSlots, DateTime zeDayInMRSTime)
        {
            foreach (var item in standSlots)
            {
                if (theSlotIsInThatDay(item, zeDayInMRSTime) && getCategoryOfStandInSlot(item) == "Abris")
                {
                    return true;
                }

            }

            return false;
        }

  
       

        public static bool theSlotIsInThatDay(StandSlot item, DateTime zeDayInMRSTime)
        {

            DateTime startOfSlotINMRSTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.ParseExact(getPropertyOfSlot(item, "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture), MRSTimeZone).Date;
            DateTime endOfSlotInMRSTime = TimeZoneInfo.ConvertTimeFromUtc(DateTime.ParseExact(getPropertyOfSlot(item, "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture), MRSTimeZone).Date;

            if (startOfSlotINMRSTime== zeDayInMRSTime || endOfSlotInMRSTime == zeDayInMRSTime
                ||(startOfSlotINMRSTime < zeDayInMRSTime && endOfSlotInMRSTime > zeDayInMRSTime))
            {
                return true;
            }

            return false;
        }

   
       

      
        List<DateTime> myTimesWhenTheSlotsNeedToBeSplit = new List<DateTime>();


        private void doTheDurationCalculationForAllMovements(Guid guid, Movement myMovement)
        {
            myTimesWhenTheSlotsNeedToBeSplit.Clear();


            if (myMovement.DepartureFlight == null || myMovement.ArrivalFlight == null
               || !thatFlightHasBlockTime(myMovement.ArrivalFlight) || !thatFlightHasBlockTime(myMovement.DepartureFlight))
            {


                return;
            }

            DateTime myAIBT = DateTime.ParseExact(FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, Configuration.widgetParameters["AIBT"].ToString()), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
            DateTime myAOBT = DateTime.ParseExact(FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, Configuration.widgetParameters["AOBT"].ToString()), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);



            if (myAOBT < myAIBT)
            {
                LogManager.TraceLog(Level.Info, "CustomServiceManager", "doTheDurationCalculationForMoreThan6Tons", guid.ToString(), "Movement AOBT < AIBT");
                return;
            }


            // determine the number of transitions And checks if we need to split some slots accordingly.

            int numberOfTransitionDayNight = 0;



            for (DateTime zeTime = myAIBT.AddMinutes(1); zeTime < myAOBT; zeTime = zeTime.AddMinutes(1))
            {
                if (TimeZoneInfo.ConvertTimeFromUtc(zeTime, MRSTimeZone).ToString("HHmm", CultureInfo.InvariantCulture) == getMRSParameterByDate("eveningNightTransition",myAIBT).ToString()
                    || TimeZoneInfo.ConvertTimeFromUtc(zeTime, MRSTimeZone).ToString("HHmm", CultureInfo.InvariantCulture) == getMRSParameterByDate("morningDayTransition",myAIBT).ToString())
                {
                    numberOfTransitionDayNight++;

                    myTimesWhenTheSlotsNeedToBeSplit.Add(zeTime);
                }

            }

            // Below Create A list of Slots that are not across the Day/Night Transition

            List<StandSlot> myStandSlotsExtended = new List<StandSlot>();
            List<StandSlot> myStandSlotsExtendedCopyToCalculateExonerations = new List<StandSlot>();


            if (myMovement.DepartureFlight.FlightState.StandSlots != null)
            {
                foreach (StandSlot myStandSlot in myMovement.DepartureFlight.FlightState.StandSlots)
                {
                    DateTime startOfSlot = DateTime.ParseExact(getPropertyOfSlot(myStandSlot, "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                    DateTime endOfSlot = DateTime.ParseExact(getPropertyOfSlot(myStandSlot, "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);


                    if (!isThereADayNightTransitionBetween(startOfSlot, endOfSlot)) //simple case where the slot is not across DayNight
                    {
                        myStandSlotsExtended.Add(myStandSlot);
                    }
                    else
                    {
                        DateTime startOfNewSlot = startOfSlot; // this stores the start of slot to be created, the end of that slot will be time when there is a transition DayNight

                        for (DateTime zeTime = startOfSlot; zeTime <= endOfSlot; zeTime = zeTime.AddMinutes(1))
                        {
                            string zeTimeInLocalTime = TimeZoneInfo.ConvertTimeFromUtc(zeTime, MRSTimeZone).ToString("HHmm", CultureInfo.InvariantCulture);



                            if (zeTimeInLocalTime == getMRSParameterByDate("eveningNightTransition",myAIBT).ToString()
                   || zeTimeInLocalTime ==getMRSParameterByDate("morningDayTransition",myAIBT).ToString())
                            {
                                if (startOfNewSlot != zeTime)
                                {
                                    StandSlot standSlotToAdd = new StandSlot
                                    {
                                        Stand = myStandSlot.Stand,
                                        Value = new Sita.AMSClient.MSMQNotifications.PropertyValue[]
                                                                        {
                                        new Sita.AMSClient.MSMQNotifications.PropertyValue{propertyName = "StartTime", Value = startOfNewSlot.ToString("yyyy-MM-ddTHH:mm:00")},
                                        new Sita.AMSClient.MSMQNotifications.PropertyValue{propertyName = "EndTime", Value = zeTime.ToString("yyyy-MM-ddTHH:mm:00")}
                                                                        }
                                    };

                                    myStandSlotsExtended.Add(standSlotToAdd);

                                    startOfNewSlot = zeTime.AddMinutes(0);
                                }



                            }

                        }

                        if (startOfNewSlot != endOfSlot)
                        {
                            StandSlot LastStandSlotToAdd = new StandSlot
                            {
                                Stand = myStandSlot.Stand,
                                Value = new Sita.AMSClient.MSMQNotifications.PropertyValue[]
                                  {
                                        new Sita.AMSClient.MSMQNotifications.PropertyValue{propertyName = "StartTime", Value = startOfNewSlot.ToString("yyyy-MM-ddTHH:mm:00")},
                                        new Sita.AMSClient.MSMQNotifications.PropertyValue{propertyName = "EndTime", Value = endOfSlot.ToString("yyyy-MM-ddTHH:mm:00")}
                                  }
                            };

                            myStandSlotsExtended.Add(LastStandSlotToAdd);
                        }
                    }
                }

                // Splits the slots with the franchise and these without


                myStandSlotsExtendedCopyToCalculateExonerations.AddRange(myStandSlotsExtended);

                // establish what the franchise should be

                List<StandSlot> myStandSlotsThatAreCoveredWithinFranchise = new List<StandSlot>();

                ApplyTheFranchiseToTheseStandSlots(myAIBT, myStandSlotsExtended, out myStandSlotsThatAreCoveredWithinFranchise); // shaves off the applicable franchise in a list of slots, starting from the AIBT

                // Deal with the slots that are during the franchise first

                int
                FranchiseConsumedMinContactTrafic = 0,
                FranchiseConsumedMinContactGarage = 0,
                FranchiseConsumedMinLargeTrafic = 0,
                FranchiseConsumedMinLargeGarage = 0,
                FranchiseConsumedMinRestreintTrafic = 0,
                FranchiseConsumedMinRestreintGarage = 0,
                FranchiseConsumedMinAbris = 0,
                FranchiseConsumedMinPrivatif = 0;

                foreach (StandSlot myStandSlot in myStandSlotsThatAreCoveredWithinFranchise)
                {

                    string standCategoryFranchiseTime = getCategoryOfStandInSlot(myStandSlot);
                    bool isDuringTheDayFlag = isDuringTheday(DateTime.ParseExact(getPropertyOfSlot(myStandSlot, "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture), myAIBT);

                    string Cat1 = "Contact";
                    string Cat2 = "Large";
                    string Cat3 = "Restreint";
                    string Cat4 = "Abris";
                    string Cat5 = "Privatif";


                    if (isDuringTheDayFlag)
                    {
                        if (standCategoryFranchiseTime == Cat1) FranchiseConsumedMinContactTrafic = FranchiseConsumedMinContactTrafic + addSlotMinutes(myStandSlot);
                        else if (standCategoryFranchiseTime == Cat2) FranchiseConsumedMinLargeTrafic = FranchiseConsumedMinLargeTrafic + addSlotMinutes(myStandSlot);
                        else if (standCategoryFranchiseTime == Cat3) FranchiseConsumedMinRestreintTrafic = FranchiseConsumedMinRestreintTrafic + addSlotMinutes(myStandSlot);
                        else if (standCategoryFranchiseTime == Cat4) FranchiseConsumedMinAbris = FranchiseConsumedMinAbris + addSlotMinutes(myStandSlot);
                        else if (standCategoryFranchiseTime == Cat5) FranchiseConsumedMinPrivatif = FranchiseConsumedMinPrivatif + addSlotMinutes(myStandSlot);

                    }
                    else
                    {
                         if (standCategoryFranchiseTime == Cat1) FranchiseConsumedMinContactGarage = FranchiseConsumedMinContactGarage + addSlotMinutes(myStandSlot);
                        else if (standCategoryFranchiseTime == Cat2) FranchiseConsumedMinLargeGarage = FranchiseConsumedMinLargeGarage + addSlotMinutes(myStandSlot);
                        else if (standCategoryFranchiseTime == Cat3) FranchiseConsumedMinRestreintGarage = FranchiseConsumedMinRestreintGarage + addSlotMinutes(myStandSlot);
                        else if (standCategoryFranchiseTime == Cat4) FranchiseConsumedMinAbris = FranchiseConsumedMinAbris + addSlotMinutes(myStandSlot);
                        else if (standCategoryFranchiseTime == Cat5) FranchiseConsumedMinPrivatif = FranchiseConsumedMinPrivatif + addSlotMinutes(myStandSlot);

                    }

                }




                // at this stage we have a list of slots with the franchise eliminated

                //    if (myStandSlotsExtended.Count == 0) return; this prevented from resetting the calculation if there was no slot after the franchise. 3oct 2019



                int
                    DurationMinContactTraffic = 0,
                    DurationMinContactGarage = 0,
                    DurationMinLargeTraffic = 0,
                    DurationMinLargeGarage = 0,
                    DurationMinRestreintTraffic = 0,
                    DurationMinRestreintGarage = 0,
                    DurationMinAbris = 0,
                    DurationMinPrivatif = 0,
                    DurationHourContactTraffic = 0,
                    DurationHourContactGarage = 0,
                    DurationHourLargeTraffic = 0,
                    DurationHourLargeGarage = 0,
                    DurationHourRestreintTraffic = 0,
                    DurationHourRestreintGarage = 0,
                    DurationHourAbris = 0,
                    DurationHourPrivatif = 0,


                    memoryCarryOverContactTraffic = 0,
                   memoryCarryOverContactGarage = 0,
                    memoryCarryOverLargeTraffic = 0,
                   memoryCarryOverLargeGarage = 0,
                   memoryCarryOverRestreintTraffic = 0,
                   memoryCarryOverRestreintGarage = 0,
                   memoryCarryOverAbris = 0,
                   memoryCarryOverPrivatif = 0
                   ;






                string standCategory = "";

                // then process all the slots
                foreach (StandSlot myStandSlot in myStandSlotsExtended)
                {

                    standCategory = getCategoryOfStandInSlot(myStandSlot);
                    bool isDuringTheDayFlag = isDuringTheday(DateTime.ParseExact(getPropertyOfSlot(myStandSlot, "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture), myAIBT);

                    string Cat1 = "Contact";
                    string Cat2 = "Large";
                    string Cat3 = "Restreint";
                    string Cat4 = "Abris";
                    string Cat5 = "Privatif";


                    if (isDuringTheDayFlag)
                    {
                        if (standCategory == Cat1) DurationMinContactTraffic = DurationMinContactTraffic + addSlotMinutes(myStandSlot);
                        else if (standCategory == Cat2) DurationMinLargeTraffic = DurationMinLargeTraffic + addSlotMinutes(myStandSlot);
                        else if (standCategory == Cat3) DurationMinRestreintTraffic = DurationMinRestreintTraffic + addSlotMinutes(myStandSlot);
                        else if (standCategory == Cat4) DurationMinAbris = DurationMinAbris + addSlotMinutes(myStandSlot);
                        else if (standCategory == Cat5) DurationMinPrivatif = DurationMinPrivatif + addSlotMinutes(myStandSlot);

                    }
                    else
                    {
                        if (standCategory == Cat1) DurationMinContactGarage = DurationMinContactGarage + addSlotMinutes(myStandSlot);
                        else if (standCategory == Cat2) {

                            int mintoadd = addSlotMinutes(myStandSlot);
                            DurationMinLargeGarage = DurationMinLargeGarage + mintoadd; }
                        else if (standCategory == Cat3) DurationMinRestreintGarage = DurationMinRestreintGarage + addSlotMinutes(myStandSlot);
                        else if (standCategory == Cat4) DurationMinAbris = DurationMinAbris + addSlotMinutes(myStandSlot);
                        else if (standCategory == Cat5) DurationMinPrivatif = DurationMinPrivatif + addSlotMinutes(myStandSlot);

                    }

                }

                // now calculation of hours.


                // get the pririties

                string[] prioritiesForHourCalculation = getMRSParameterByDate("HourRulesPriority",myAIBT).ToString().Split(',');

                int carryOver = 0;

                foreach (string item in prioritiesForHourCalculation)
                {
                    int countInQuestion = 0;

                    switch (item)
                    {
                        case "ContactTrafic":
                            countInQuestion = DurationMinContactTraffic;
                            break;
                        case "ContactGarage":
                            countInQuestion = DurationMinContactGarage;
                            break;
                        case "LargeTrafic":
                            countInQuestion = DurationMinLargeTraffic;
                            break;
                        case "LargeGarage":
                            countInQuestion = DurationMinLargeGarage;
                            break;
                        case "RestreintTrafic":
                            countInQuestion = DurationMinRestreintTraffic;
                            break;
                        case "RestreintGarage":
                            countInQuestion = DurationMinRestreintGarage;
                            break;
                        case "Abris":
                            countInQuestion = DurationMinAbris;
                            break;
                        case "Privatif":
                            countInQuestion = DurationMinPrivatif;
                            break;

                        default:
                            break;
                    }

                    countInQuestion = countInQuestion - carryOver; // to add what's left of the previous count.


                    float myNumber = countInQuestion;
                    myNumber = myNumber / 60;
                    int numberOfHours = (int)Math.Ceiling(myNumber);
                    carryOver = (numberOfHours * 60) - countInQuestion;

                    switch (item)
                    {
                        case "ContactTrafic":
                            DurationHourContactTraffic = numberOfHours;
                            memoryCarryOverContactTraffic = carryOver;
                            break;
                        case "ContactGarage":
                            DurationHourContactGarage = numberOfHours;
                            memoryCarryOverContactGarage = carryOver;
                            break;
                        case "LargeTrafic":
                            DurationHourLargeTraffic = numberOfHours;
                            memoryCarryOverLargeTraffic = carryOver;
                            break;
                        case "LargeGarage":
                            DurationHourLargeGarage = numberOfHours;
                            memoryCarryOverLargeGarage = carryOver;
                            break;
                        case "RestreintTrafic":
                            DurationHourRestreintTraffic = numberOfHours;
                            memoryCarryOverRestreintTraffic = carryOver;
                            break;
                        case "RestreintGarage":
                            DurationHourRestreintGarage = numberOfHours;
                            memoryCarryOverRestreintGarage = carryOver;
                            break;
                        case "Abris":
                            DurationHourAbris = numberOfHours;
                            memoryCarryOverAbris = carryOver;
                            break;
                        case "Privatif":
                            DurationHourPrivatif = numberOfHours;
                            memoryCarryOverPrivatif = carryOver;
                            break;

                        default:
                            break;
                    }


                }



                // now ready to prepare what needs to be send

                List<Sita.AMSClient.AMSSoapService.PropertyValue> myListOfPVToSend = new List<Sita.AMSClient.AMSSoapService.PropertyValue>
                    {

                    // the real minutes - chnaged the 3oct19

                      new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinContactTraficCF"].ToString(),
                            valueField = (DurationMinContactTraffic + FranchiseConsumedMinContactTrafic).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinContactGarageCF"].ToString(),
                            valueField =(DurationMinContactGarage + FranchiseConsumedMinContactGarage).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinLargeTraficCF"].ToString(),
                            valueField =(DurationMinLargeTraffic + FranchiseConsumedMinLargeTrafic).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinLargeGarageCF"].ToString(),
                            valueField =(DurationMinLargeGarage+ FranchiseConsumedMinLargeGarage).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinRestreintTraficCF"].ToString(),
                            valueField =(DurationMinRestreintTraffic + FranchiseConsumedMinRestreintTrafic).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinRestreintGarageCF"].ToString(),
                            valueField =(DurationMinRestreintGarage+ FranchiseConsumedMinRestreintGarage).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinAbrisCF"].ToString(),
                            valueField =(DurationMinAbris + FranchiseConsumedMinAbris).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinPrivatifCF"].ToString(),
                            valueField =(DurationMinPrivatif + FranchiseConsumedMinPrivatif).ToString()
                        },



                          // the franchise minutes - chnaged the 3oct19

                      new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Contact_Trafic_CF"].ToString(),
                            valueField = (FranchiseConsumedMinContactTrafic).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Contact_Garage_CF"].ToString(),
                            valueField =( FranchiseConsumedMinContactGarage).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Large_Trafic_CF"].ToString(),
                            valueField =( FranchiseConsumedMinLargeTrafic).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Large_Garage_CF"].ToString(),
                            valueField =( FranchiseConsumedMinLargeGarage).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Restreint_Trafic_CF"].ToString(),
                            valueField =(  FranchiseConsumedMinRestreintTrafic).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Restreint_Garage_CF"].ToString(),
                            valueField =( FranchiseConsumedMinRestreintGarage).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Abris_CF"].ToString(),
                            valueField =( FranchiseConsumedMinAbris).ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Privatif_CF"].ToString(),
                            valueField =(FranchiseConsumedMinPrivatif).ToString()
                        },








                        // now the hours




                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourContactTraficCF"].ToString(),
                            valueField = DurationHourContactTraffic.ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourContactGarageCF"].ToString(),
                            valueField =DurationHourContactGarage.ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourLargeTraficCF"].ToString(),
                            valueField =DurationHourLargeTraffic.ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourLargeGarageCF"].ToString(),
                            valueField =DurationHourLargeGarage.ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourRestreintTraficCF"].ToString(),
                            valueField =DurationHourRestreintTraffic.ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourRestreintGarageCF"].ToString(),
                            valueField =DurationHourRestreintGarage.ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourAbrisCF"].ToString(),
                            valueField =DurationHourAbris.ToString()
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourPrivatifCF"].ToString(),
                            valueField =DurationHourPrivatif.ToString()
                        }


                };



                int memoryDurationHourContactGarage = DurationHourContactGarage;
                int memoryDurationHourLargeGarage = DurationHourLargeGarage;
                int memoryDurationHourRestreintGarage = DurationHourRestreintGarage;

                // now we need to redo the calculation for the night exoneration, by eliminating the slots that correpond to a night with more than the threshold.


                int numberOfNightsWhenExoApplies = 0;

                List<StandSlot> myRemainingStandSlot = new List<StandSlot>();
                DurationMinContactGarage = 0;
                DurationMinLargeGarage = 0;
                DurationMinRestreintGarage = 0;

                myRemainingStandSlot = EliminateSlotsThatCorrespondToNightStationnementCriteria(myStandSlotsExtendedCopyToCalculateExonerations,myAIBT,
                    out DurationMinContactGarage,
                    out DurationMinLargeGarage,
                    out DurationMinRestreintGarage);







                // now calculation of hours.

                DurationHourContactGarage = 0;
                DurationHourLargeGarage = 0;
                DurationHourRestreintGarage = 0;

                // get the pririties

                int carryOverToSubstractInNextStep = 0;


                carryOver = 0;

                foreach (string item in prioritiesForHourCalculation)
                {
                    int countInQuestion = 0;

                    switch (item)
                    {

                        case "ContactGarage":
                            if (DurationMinContactGarage == 0)
                            {
                                countInQuestion = 0;
                            }
                            else countInQuestion = DurationMinContactGarage - carryOverToSubstractInNextStep - FranchiseConsumedMinContactGarage;
                            break;

                        case "LargeGarage":
                            if (DurationMinLargeGarage == 0)
                            {
                                countInQuestion = 0;
                            }
                            else countInQuestion = DurationMinLargeGarage - carryOverToSubstractInNextStep - FranchiseConsumedMinLargeGarage;
                            break;

                        case "RestreintGarage":

                            if (DurationMinRestreintGarage == 0)
                            {
                                countInQuestion = 0;
                            }
                            else countInQuestion = DurationMinRestreintGarage - carryOverToSubstractInNextStep - FranchiseConsumedMinRestreintGarage;
                            break;

                        default:
                            break;
                    }

                    countInQuestion = countInQuestion - carryOver; // to add what's left of the previous count.

                    float myNumber = countInQuestion;
                    myNumber = myNumber / 60;
                    int numberOfHours = (int)Math.Ceiling(myNumber);
                    carryOver = (numberOfHours * 60) - countInQuestion;

                    switch (item)
                    {
                        case "ContactTrafic":
                            carryOverToSubstractInNextStep = memoryCarryOverContactTraffic;
                            break;

                        case "ContactGarage":
                            DurationHourContactGarage = numberOfHours;
                            carryOverToSubstractInNextStep = 0;

                            break;

                        case "LargeTrafic":
                            carryOverToSubstractInNextStep = memoryCarryOverLargeTraffic;

                            break;

                        case "LargeGarage":
                            DurationHourLargeGarage = numberOfHours;
                            carryOverToSubstractInNextStep = 0;

                            break;

                        case "RestreintTrafic":
                            carryOverToSubstractInNextStep = memoryCarryOverRestreintTraffic;

                            break;

                        case "RestreintGarage":
                            DurationHourRestreintGarage = numberOfHours;
                            carryOverToSubstractInNextStep = 0;

                            break;

                        default:
                            break;
                    }


                }


                // add the exonerated fields into the list to send

                myListOfPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                {
                    propertyNameField = Configuration.widgetParameters["DurationMinContactGarageEXOCF"].ToString(),
                    valueField = DurationMinContactGarage.ToString()
                });

                myListOfPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                {
                    propertyNameField = Configuration.widgetParameters["DurationMinLargeGarageEXOCF"].ToString(),
                    valueField = DurationMinLargeGarage.ToString()
                });

                myListOfPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                {
                    propertyNameField = Configuration.widgetParameters["DurationMinRestreintGarageEXOCF"].ToString(),
                    valueField = DurationMinRestreintGarage.ToString()
                });

                myListOfPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                {
                    propertyNameField = Configuration.widgetParameters["DurationHourContactGarageEXOCF"].ToString(),
                    valueField = DurationHourContactGarage.ToString()
                });

                myListOfPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                {
                    propertyNameField = Configuration.widgetParameters["DurationHourLargeGarageEXOCF"].ToString(),
                    valueField = DurationHourLargeGarage.ToString()
                });

                myListOfPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                {
                    propertyNameField = Configuration.widgetParameters["DurationHourRestreintGarageEXOCF"].ToString(),
                    valueField = DurationHourRestreintGarage.ToString()
                });

                // now the calculation (4mar20)


                bool stationnementDenuitCFOutputFlag =( stationnementDeNuitCFValueMemory.ToLower() == "oui");
                bool UseTheExonerationStatNuitFlag = Configuration.widgetParameters["UseTheExonerationStatNuitFlag"].ToString().ToLower() == "true";
                bool thisFlightIsExonerated = ( FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, Configuration.widgetParameters["ExonerationStatNuitFlag"]).ToLower() == "oui");
                bool ExonerationShouldApply = (stationnementDenuitCFOutputFlag && thisFlightIsExonerated) || !UseTheExonerationStatNuitFlag;

                int durationOfExoneration = ExonerationShouldApply ? DurationHourContactGarage : 0;
                myListOfPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                {
                    propertyNameField = Configuration.widgetParameters["FacturationContactGarage"].ToString(),
                    valueField = (memoryDurationHourContactGarage - durationOfExoneration).ToString()
                });



                durationOfExoneration = ExonerationShouldApply ? DurationHourLargeGarage : 0;
                myListOfPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                {
                    propertyNameField = Configuration.widgetParameters["FacturationLargeGarage"].ToString(),
                    valueField = (memoryDurationHourLargeGarage - durationOfExoneration).ToString()
                });

                durationOfExoneration = ExonerationShouldApply ? DurationHourRestreintGarage : 0;
                myListOfPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                {
                    propertyNameField = Configuration.widgetParameters["FacturationRestreintGarage"].ToString(),
                    valueField = (memoryDurationHourRestreintGarage - durationOfExoneration).ToString()
                });





                FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, myMovement.ArrivalFlight, myListOfPVToSend);
            }



        }



        private void initializeThePVListWithAllResetValue(Guid guid, Movement myMovement)
        {
            if (myMovement.ArrivalFlight != null) // reset the arrival PVs
            {
                List<Sita.AMSClient.AMSSoapService.PropertyValue> myListOfArrivalPVToSend = new List<Sita.AMSClient.AMSSoapService.PropertyValue>
                    {

                        //new Sita.AMSClient.AMSSoapService.PropertyValue
                        //{
                        //    propertyNameField = Configuration.widgetParameters["ArrDurationPasserelle"].ToString(),
                        //    valueField = "0"
                        //}, 
                    new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField = Configuration.widgetParameters["ForfaitStat"].ToString(),
                            valueField = "0"
                        },
                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField = Configuration.widgetParameters["StatJourAbris"].ToString(),
                            valueField = "0"
                        }
                        ,
                        //new Sita.AMSClient.AMSSoapService.PropertyValue  // no longer in real time CR 21oct20. Done in batch through extension
                        //{
                        //    propertyNameField = Configuration.widgetParameters["StatJourHorsAbris"].ToString(),
                        //    valueField = "0"
                        //},

                    // the real minutes - chnaged the 3oct19

                      new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinContactTraficCF"].ToString(),  valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinContactGarageCF"].ToString(),   valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinLargeTraficCF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinLargeGarageCF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinRestreintTraficCF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinRestreintGarageCF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinAbrisCF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationMinPrivatifCF"].ToString(), valueField = "0"
                        },



                          // the franchise minutes - chnaged the 3oct19

                      new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Contact_Trafic_CF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Contact_Garage_CF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Large_Trafic_CF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Large_Garage_CF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Restreint_Trafic_CF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Restreint_Garage_CF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Abris_CF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["Franchise_Consommée_Aire_Privatif_CF"].ToString(), valueField = "0"
                        },



                        // now the hours
                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourContactTraficCF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourContactGarageCF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourLargeTraficCF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourLargeGarageCF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourRestreintTraficCF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourRestreintGarageCF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourAbrisCF"].ToString(), valueField = "0"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DurationHourPrivatifCF"].ToString(), valueField = "0"
                        },


                          new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["FacturationContactGarage"].ToString(), valueField = "0"
                        },
                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["FacturationLargeGarage"].ToString(), valueField = "0"
                        },
                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["FacturationRestreintGarage"].ToString(), valueField = "0"
                        },




                        



                          // add for indispo

                         new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField = Configuration.widgetParameters["IndispoPasserelleNordArrCF"].ToString(),
                            valueField = "Non"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField = Configuration.widgetParameters["IndispoPasserelleSudArrCF"].ToString(),
                            valueField = "Non"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField = Configuration.widgetParameters["IndispoPosteAArrCF"].ToString(),
                            valueField = "Non"
                        },

                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField = Configuration.widgetParameters["IndispoPosteCArrCF"].ToString(),
                            valueField = "Non"
                        }
                    };

            // add the exonerated fields into the list to send

                        myListOfArrivalPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField = Configuration.widgetParameters["DurationMinContactGarageEXOCF"].ToString(),
                             valueField = "0"
                        });

                        myListOfArrivalPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField = Configuration.widgetParameters["DurationMinLargeGarageEXOCF"].ToString(),
                             valueField = "0"
                        });

                        myListOfArrivalPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField = Configuration.widgetParameters["DurationMinRestreintGarageEXOCF"].ToString(),
                             valueField = "0"
                        });

                        myListOfArrivalPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField = Configuration.widgetParameters["DurationHourContactGarageEXOCF"].ToString(),
                             valueField = "0"
                        });

                        myListOfArrivalPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField = Configuration.widgetParameters["DurationHourLargeGarageEXOCF"].ToString(),
                             valueField = "0"
                        });

                        myListOfArrivalPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField = Configuration.widgetParameters["DurationHourRestreintGarageEXOCF"].ToString(),
                            valueField = "0"
                        });








            FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, myMovement.ArrivalFlight, myListOfArrivalPVToSend);
        }

            if (myMovement.DepartureFlight!= null)
            {
                List<Sita.AMSClient.AMSSoapService.PropertyValue> myListOfDeparturePVToSend = new List<Sita.AMSClient.AMSSoapService.PropertyValue>
                    {

                        //new Sita.AMSClient.AMSSoapService.PropertyValue
                        //{
                        //    propertyNameField = Configuration.widgetParameters["DepDurationPasserelle"].ToString(),
                        //    valueField = "0"
                        //},

                        // add for indispo


                    // modif 4dec
                        // new Sita.AMSClient.AMSSoapService.PropertyValue
                        //{
                        //    propertyNameField = Configuration.widgetParameters["IndispoPasserelleNordDepCF"].ToString(),
                        //    valueField = "Non"
                        //}, new Sita.AMSClient.AMSSoapService.PropertyValue
                        //{
                        //    propertyNameField = Configuration.widgetParameters["IndispoPasserelleSudDepCF"].ToString(),
                        //    valueField = "Non"
                        //}, new Sita.AMSClient.AMSSoapService.PropertyValue
                        //{
                        //    propertyNameField = Configuration.widgetParameters["IndispoPosteADepCF"].ToString(),
                        //    valueField = "Non"
                        //}, new Sita.AMSClient.AMSSoapService.PropertyValue
                        //{
                        //    propertyNameField = Configuration.widgetParameters["IndispoPosteCDepCF"].ToString(),
                        //    valueField = "Non"
                        //}


                };
                  FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, myMovement.DepartureFlight, myListOfDeparturePVToSend);
            }

            
        }

        // below shaves off the applicable franchise in a list of slots, starting from the AIBT
        public static void ApplyTheFranchiseToTheseStandSlots(DateTime myAIBT, List<StandSlot> myStandSlotsExtended, out List<StandSlot> myStandSlotsThatAreConveredThisFranchise)
        {
            myStandSlotsThatAreConveredThisFranchise = new List<StandSlot>();

            StandSlot myFirstStandSlot = myStandSlotsExtended[0];


            string standCategory = getCategoryOfStandInSlot(myFirstStandSlot);

            int franchise = 0;

            switch (standCategory)
            {
                case "Abris":
                    franchise = Convert.ToInt32(getMRSParameterByDate("Franchise_Aire_Abris",myAIBT));
                    break;

                case "Contact":
                    franchise = Convert.ToInt32(getMRSParameterByDate("Franchise_Aire_Contact",myAIBT));
                    break;

                case "Restreint":
                    franchise = Convert.ToInt32(getMRSParameterByDate("Franchise_Aire_Restreint",myAIBT));
                    break;

                case "Large":
                    franchise = Convert.ToInt32(getMRSParameterByDate("Franchise_Aire_Large",myAIBT));
                    break;

                case "Privatif":
                    franchise = Convert.ToInt32(getMRSParameterByDate("Franchise_Aire_Privatif",myAIBT));
                    break;

                default:
                    break;
            }


            // 1st copy in another list

            List<StandSlot> myTempList = new List<StandSlot>();
            foreach (StandSlot myStandSlot in myStandSlotsExtended) myTempList.Add(myStandSlot);

            // then truncate

            myStandSlotsExtended.Clear();

            DateTime myAIBTPlusFranchise = myAIBT.AddMinutes(franchise);

            foreach (StandSlot myStandSlot in myTempList)
            {
                DateTime startOfSlot = DateTime.ParseExact(getPropertyOfSlot(myStandSlot, "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                DateTime endOfSlot = DateTime.ParseExact(getPropertyOfSlot(myStandSlot, "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);


                if (startOfSlot >= myAIBTPlusFranchise)
                {
                    myStandSlotsExtended.Add(myStandSlot);
                }
                else
                {
                    if (endOfSlot > myAIBTPlusFranchise)
                    {
                        myStandSlotsExtended.Add(new StandSlot
                        {
                            Stand = myStandSlot.Stand,
                            Value = new Sita.AMSClient.MSMQNotifications.PropertyValue[] {
                            new Sita.AMSClient.MSMQNotifications.PropertyValue{propertyName = "StartTime", Value = myAIBTPlusFranchise.ToString("yyyy-MM-ddTHH:mm:00",CultureInfo.InvariantCulture) },
                            new Sita.AMSClient.MSMQNotifications.PropertyValue{propertyName = "EndTime", Value =getPropertyOfSlot(myStandSlot, "EndTime")}

                            }
                        });



                        myStandSlotsThatAreConveredThisFranchise.Add(new StandSlot
                        {
                            Stand = myStandSlot.Stand,
                            Value = new Sita.AMSClient.MSMQNotifications.PropertyValue[] {
                            new Sita.AMSClient.MSMQNotifications.PropertyValue{propertyName = "StartTime", Value =getPropertyOfSlot(myStandSlot, "StartTime") },
                            new Sita.AMSClient.MSMQNotifications.PropertyValue{propertyName = "EndTime", Value = myAIBTPlusFranchise.ToString("yyyy-MM-ddTHH:mm:00",CultureInfo.InvariantCulture)}

                            }
                        });
                    }
                    else
                    {
                        myStandSlotsThatAreConveredThisFranchise.Add(myStandSlot);
                    }
                }


            }

            return;
        }

    
        private List<StandSlot> EliminateSlotsThatCorrespondToNightStationnementCriteria(List<StandSlot> myStandSlotsToReturn, DateTime myAIBT,out int DurationMinContactGarage,
                    out int DurationMinLargeGarage,
                    out int DurationMinRestreintGarage)
        {

            List<StandSlot> slotsToBeDeleted = new List<StandSlot>();
            DurationMinContactGarage = 0;
            DurationMinLargeGarage = 0;
            DurationMinRestreintGarage = 0;

            int nbSlots = myStandSlotsToReturn.Count;

            for (int i = 0; i < nbSlots; i++)
            {
                DateTime fromTime = DateTime.ParseExact(getPropertyOfSlot(myStandSlotsToReturn[i], "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                DateTime toTime = DateTime.ParseExact(getPropertyOfSlot(myStandSlotsToReturn[i], "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);


            
                if (!isDuringTheday(fromTime, myAIBT)   || TimeZoneInfo.ConvertTimeFromUtc( fromTime, MRSTimeZone).ToString("HHmm",CultureInfo.InvariantCulture) == getMRSParameterByDate("eveningNightTransition", fromTime).ToString())
                {
                    List<StandSlot> slotsForThatNight = new List<StandSlot>(); // will store the list of slots for the night that's starting 
                    slotsForThatNight.Add(myStandSlotsToReturn[i]);
                    DateTime endOfThatNight = toTime; // will store the end of the night, so that we can check the duration during that night

                    // need to check with the following slots if the sum is more than the threshold

                    for (int k = 1; k <= nbSlots-i-1; k++)
                    {

                        DateTime fromTimeFlwgSlot = DateTime.ParseExact(getPropertyOfSlot(myStandSlotsToReturn[i +k], "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                        if (!isDuringTheday(fromTimeFlwgSlot, myAIBT))

                        {
                            slotsForThatNight.Add(myStandSlotsToReturn[i+k]);
                            endOfThatNight = DateTime.ParseExact(getPropertyOfSlot(myStandSlotsToReturn[i + k], "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                        }
                        else
                        {
                            i = i + k-1;
                            break; // need to get out as we are now druing the next day, otherwise we double count all following nights... 4 oct 19 bug
                        }

                    }

                    // check the duration of the night, and if bigger than the threshold, add the slots of that night to the list of slots that need to be deleted.

                    if ((endOfThatNight-fromTime).TotalMinutes >= Convert.ToInt32(getMRSParameterByDate("stationnementDeNuitThreshold", myAIBT).ToString()))
                    {
                        string Cat1 = "Contact";
                        string Cat2 = "Large";
                        string Cat3 = "Restreint";

                        foreach (var myStandSlot in slotsForThatNight) // now we need to add the duration of slots
                        {

                            string standCategory = getCategoryOfStandInSlot(myStandSlot);
                        
                           

                                if (standCategory == Cat1) DurationMinContactGarage = DurationMinContactGarage + addSlotMinutes(myStandSlot);
                                if (standCategory == Cat2) DurationMinLargeGarage = DurationMinLargeGarage + addSlotMinutes(myStandSlot);
                                if (standCategory == Cat3) DurationMinRestreintGarage = DurationMinRestreintGarage + addSlotMinutes(myStandSlot);


                        }

                        
                    }
                   
                }


              

            }

            //// now delete the slots that need deletion from the list


            //foreach (var item in slotsToBeDeleted)
            //{
            //    myStandSlotsExtended.Remove(item);
            //}

            return myStandSlotsToReturn;


        }

        private int addSlotMinutes(StandSlot myStandSlot)
            {
                DateTime fromTime = DateTime.ParseExact(getPropertyOfSlot(myStandSlot, "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                DateTime toTime = DateTime.ParseExact(getPropertyOfSlot(myStandSlot, "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                int numberOfMinutesSpent = (int)(toTime - fromTime).TotalMinutes;

                return numberOfMinutesSpent;
            }

            public static string getCategoryOfStandInSlot(StandSlot myStanSlot)
        {
            if (myStanSlot.Stand == null) return "";

            string myStandKey =   myStanSlot.Stand.Value[0].Value;
            if (myStandDico.ContainsKey(myStandKey))
            {
                return myStandDico[myStandKey];
            }
            return "";

        }


        public static string getPropertyOfSlot(Slot item, string v)
        {
            foreach (Sita.AMSClient.MSMQNotifications.PropertyValue PV in item.Value)
            {
                if (PV.propertyName == v) return PV.Value;

            }
            return "";
        }


        bool isThereADayNightTransitionBetween(DateTime startOfSlot, DateTime endOfSlot)
        {

            foreach (DateTime timeToCheck in myTimesWhenTheSlotsNeedToBeSplit)
            {
                if (timeToCheck >= startOfSlot && timeToCheck <= endOfSlot) return true;
            }

            return false;
        }


        string stationnementDeNuitCFValueMemory = "";
        private void doTheStationnementDeNuitJob(Guid guid, Movement myMovement)
        {
            if (myMovement.DepartureFlight == null || myMovement.ArrivalFlight == null
                || !thatFlightHasBlockTime(myMovement.ArrivalFlight) || !thatFlightHasBlockTime(myMovement.DepartureFlight))

            {   
                if (myMovement.ArrivalFlight!= null && myMovement.DepartureFlight != null && thatFlightHasBlockTime(myMovement.ArrivalFlight)) // no block time for departure
                {

                    // need to do the normal process but for onyl the slots that are ended in the past

                    string valueThatShouldBe = "Non";

                    if (thatMovementCompliesWithTheStationnementDeNuitRule(guid, myMovement, true)) valueThatShouldBe = "Oui";

                    // 3 oct 19, MRS demande de mettre la valuer sur le CF arrivée.

                    FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, myMovement.ArrivalFlight, new List<Sita.AMSClient.AMSSoapService.PropertyValue>
                    {
                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["stationnementDeNuitCF"].ToString(),
                            valueField = valueThatShouldBe
                        }

                    });

                    stationnementDeNuitCFValueMemory = valueThatShouldBe;
                }
                

                // need to check if we need to reset anything that would have been be set in the past

                else  if (myMovement.ArrivalFlight != null)
                {
                    // 3 oct 19, MRS demande de mettre la valuer sur le CF arrivée.

                    FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, myMovement.ArrivalFlight, new List<Sita.AMSClient.AMSSoapService.PropertyValue>
                    {
                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["stationnementDeNuitCF"].ToString(),
                            valueField = "Non"
                        }

                    });
                    stationnementDeNuitCFValueMemory = "Non";
                }


            }
            else // do the normal work
            {
                string valueThatShouldBe = "Non";

                if (thatMovementCompliesWithTheStationnementDeNuitRule(guid, myMovement, false)) valueThatShouldBe = "Oui";


                FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, myMovement.ArrivalFlight, new List<Sita.AMSClient.AMSSoapService.PropertyValue>
                    {
                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["stationnementDeNuitCF"].ToString(),
                            //propertyNameField =Configuration.widgetParameters["ARR_S_STAT_NUIT) "].ToString(),
                            valueField = valueThatShouldBe
                        }

                    });
                stationnementDeNuitCFValueMemory = valueThatShouldBe;
            }



        }

        private bool thatMovementCompliesWithTheStationnementDeNuitRule(Guid guid, Movement myMovement, bool checkThePastOnly)
        {
            DateTime myAIBT = DateTime.ParseExact(FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, Configuration.widgetParameters["AIBT"].ToString()), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
            DateTime myEndOfVerification = DateTime.MinValue;
            if (checkThePastOnly)
            {
                 myEndOfVerification = DateTime.Now;
            }
            else
            {
                 myEndOfVerification = DateTime.ParseExact(FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, Configuration.widgetParameters["AOBT"].ToString()), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                // is == AOBT unless we look at the past only.  
            }



            if (myEndOfVerification < myAIBT)
            {
                LogManager.TraceLog(Level.Info, "CustomServiceManager" //className
      , "thatMovementCompliesWithTheStationnementDeNuitRule", guid.ToString(),
      "Movement AOBT < AIBT");
                return false;
            }


            // get the transtions Day/Night and NightDay in between AIBT and AOBT

         
            string morningDayTransition =getMRSParameterByDate("morningDayTransition",myAIBT).ToString();
            string eveningNightTransition = getMRSParameterByDate("eveningNightTransition",myAIBT).ToString();

            int threshold = Convert.ToInt32(getMRSParameterByDate("stationnementDeNuitThreshold",myAIBT).ToString());

            bool startIsAtNight = !isDuringTheday(myAIBT, myAIBT);

            DateTime lastTransition = myAIBT;

            for (DateTime zeTime = myAIBT.AddMinutes(1); zeTime <= myEndOfVerification; zeTime = zeTime.AddMinutes(1))
            {
                TimeSpan durationSinceLastTransition = zeTime - lastTransition;
                if (startIsAtNight && (durationSinceLastTransition.TotalMinutes >= threshold))
                {
                    return true; // we found a duration more than threshold!!
                }

                if (TimeZoneInfo.ConvertTimeFromUtc(zeTime, MRSTimeZone).ToString("HHmm", CultureInfo.InvariantCulture) == morningDayTransition)
                {
                    lastTransition = zeTime;
                    startIsAtNight = false;
                }
                else if (TimeZoneInfo.ConvertTimeFromUtc(zeTime, MRSTimeZone).ToString("HHmm", CultureInfo.InvariantCulture) == eveningNightTransition)
                {
                    lastTransition = zeTime;
                    startIsAtNight = true;

                }

            }


            return false;

        }


        // the change of parameter is relative to the AIBT
        bool isDuringTheday(DateTime dateTimeToCheck, DateTime myAIBT)
        {

            TimeSpan morningTransitionTimeSpan = new TimeSpan(Convert.ToInt32(getMRSParameterByDate("morningDayTransition", myAIBT).ToString().Substring(0, 2)),
                Convert.ToInt32(getMRSParameterByDate("morningDayTransition", myAIBT).ToString().Substring(2, 2)), 0);
            TimeSpan eveningTransitionTimeSpan = new TimeSpan(Convert.ToInt32(getMRSParameterByDate("eveningNightTransition", myAIBT).ToString().Substring(0, 2)),
                          Convert.ToInt32(getMRSParameterByDate("eveningNightTransition", myAIBT).ToString().Substring(2, 2)), 0);


            DateTime morningTransitionDateTimeInMRSTimeZone = dateTimeToCheck.Date + morningTransitionTimeSpan;
            DateTime EveningTransitionDateTimeInMRSTimeZone = dateTimeToCheck.Date + eveningTransitionTimeSpan;

            dateTimeToCheck = TimeZoneInfo.ConvertTimeFromUtc(dateTimeToCheck, MRSTimeZone);

            if (dateTimeToCheck >= morningTransitionDateTimeInMRSTimeZone && dateTimeToCheck < EveningTransitionDateTimeInMRSTimeZone) return true;


            return false;
        }

        private void doThePasserelleDurationJob(Guid guid, Movement myMovement)
        {
            if (myMovement.DepartureFlight == null)
            {
                string nbTotPasserelle = FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, Configuration.widgetParameters["ArrNombreTotalPasserelle"].ToString());

                if (thatFlightHasBlockTime(myMovement.ArrivalFlight) && nbTotPasserelle != "0" && nbTotPasserelle != "") // calculate arrival only
                {

                    updateArrivalFlightWithFirstStandSlotDuration(guid, myMovement.ArrivalFlight);

                }
                else
                {
                    resetTempsPasserelleArr(guid, myMovement.ArrivalFlight);
                }


            }
            else if (myMovement.ArrivalFlight == null)
            {
                string nbTotPasserelle = FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, Configuration.widgetParameters["DepNombreTotalPasserelle"].ToString());

                if (thatFlightHasBlockTime(myMovement.DepartureFlight) && nbTotPasserelle != "0" && nbTotPasserelle != "") // calculate departure only
                {
                    updateDepartureFlightWithLastStandSlotDuration(guid, myMovement.DepartureFlight);

                }
                else
                {
                    resetTempsPasserelleDep(guid, myMovement.DepartureFlight);
                }
            }
            else // linked movement
            {
                if (thatFlightHasBlockTime(myMovement.ArrivalFlight) && thatFlightHasBlockTime(myMovement.DepartureFlight)) // calculate departure only
                {
                    if (myMovement.ArrivalFlight.FlightState.StandSlots != null && myMovement.ArrivalFlight.FlightState.StandSlots.Length != 0)
                    {

                        string nbTotPasserelle = FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, Configuration.widgetParameters["ArrNombreTotalPasserelle"].ToString());

                        if (thatFlightHasBlockTime(myMovement.ArrivalFlight) && nbTotPasserelle != "0" && nbTotPasserelle != "") // calculate arrival only
                        {

                            updateArrivalFlightWithFirstStandSlotDuration(guid, myMovement.ArrivalFlight);

                        }
                        else
                        {
                            resetTempsPasserelleArr(guid, myMovement.ArrivalFlight);
                        }



                        if (myMovement.ArrivalFlight.FlightState.StandSlots.Length == 1) // pas de tractage
                        {
                            resetTempsPasserelleDep(guid, myMovement.DepartureFlight);


                        }
                        else
                        {
                            nbTotPasserelle = FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, Configuration.widgetParameters["DepNombreTotalPasserelle"].ToString());

                            if (nbTotPasserelle == "" || nbTotPasserelle == "0")
                            {
                                resetTempsPasserelleDep(guid, myMovement.DepartureFlight);
                             }
                            else
                            {
                                updateDepartureFlightWithLastStandSlotDuration(guid, myMovement.DepartureFlight);

                            }

                        }

                    }



                }
                else if (thatFlightHasBlockTime(myMovement.ArrivalFlight)) // arrival done, We need to calculate the passerelle if it is in the past
                {
                    if (myMovement.ArrivalFlight.FlightState.StandSlots!= null && myMovement.ArrivalFlight.FlightState.StandSlots.Length >1)
                    {
                        DateTime OffBlockTime = DateTime.ParseExact(getPropertyInSlot(typeof(StandSlot), myMovement.ArrivalFlight.FlightState.StandSlots[0], "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                        if (OffBlockTime <= DateTime.Now)
                        {
                            updateArrivalFlightWithFirstStandSlotDuration(guid,myMovement.ArrivalFlight);
                           

                        }
                        else
                        {
                            resetTempsPasserelleArr(guid, myMovement.ArrivalFlight);

                        }
                    }
                       
                }
                else
                {
                    resetTempsPasserelleDep(guid, myMovement.DepartureFlight);
                    resetTempsPasserelleArr(guid, myMovement.ArrivalFlight);
                }

            }
        }

        private void updateDepartureFlightWithLastStandSlotDuration(Guid guid, Flight departureFlight)
        {
            if (departureFlight.FlightState.StandSlots != null && departureFlight.FlightState.StandSlots.Length != 0)
            {
                StandSlot standSlot = departureFlight.FlightState.StandSlots[departureFlight.FlightState.StandSlots.Length - 1];

                DateTime InBlockTime = DateTime.ParseExact(getPropertyInSlot(typeof(StandSlot), standSlot, "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                DateTime OffBlockTime = DateTime.ParseExact(getPropertyInSlot(typeof(StandSlot), standSlot, "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                int duration = (int)(OffBlockTime - InBlockTime).TotalMinutes;

                FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, departureFlight, new List<Sita.AMSClient.AMSSoapService.PropertyValue>
                    {
                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DepDurationPasserelle"].ToString(),
                            valueField = duration.ToString()
                        }

                    });


            }
            else
            {
                resetTempsPasserelleDep(guid, departureFlight);

            }
        }

        private void resetTempsPasserelleDep(Guid guid, Flight departureFlight)
        {
            
                FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, departureFlight, new List<Sita.AMSClient.AMSSoapService.PropertyValue>
                    {
                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["DepDurationPasserelle"].ToString(),
                            valueField = "0"
                        }

                    });
           
        }

        private string getPropertyInSlot(Type myClass, object mySlot, string PropToFind)
        {
            Sita.AMSClient.MSMQNotifications.PropertyValue[] myPVs = null;

            if (myClass == typeof(StandSlot))
            {
                myPVs = ((StandSlot)mySlot).Value;
            }
            if (myClass == typeof(GateSlot))
            {
                myPVs = ((GateSlot)mySlot).Value;
            }

            foreach (Sita.AMSClient.MSMQNotifications.PropertyValue PV in myPVs)
            {
                if (PV.propertyName == PropToFind)
                {
                    if (PV.Value == null) return "";

                    return PV.Value;
                }
            }

            return "";
        }


        private void updateArrivalFlightWithFirstStandSlotDuration(Guid guid, Flight arrivalFlight)
        {
            if (arrivalFlight.FlightState.StandSlots != null && arrivalFlight.FlightState.StandSlots.Length != 0)
            {
                StandSlot standSlot = arrivalFlight.FlightState.StandSlots[0];

                DateTime InBlockTime = DateTime.ParseExact(getPropertyInSlot(typeof(StandSlot), standSlot, "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                DateTime OffBlockTime = DateTime.ParseExact(getPropertyInSlot(typeof(StandSlot), standSlot, "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                int duration = (int)(OffBlockTime - InBlockTime).TotalMinutes;


                if (FlightMethods.getCFValueForFlight(arrivalFlight, Configuration.widgetParameters["ArrDurationPasserelle"].ToString()) != duration.ToString())
                {
                    FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, arrivalFlight, new List<Sita.AMSClient.AMSSoapService.PropertyValue>
                    {
                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["ArrDurationPasserelle"].ToString(),
                            valueField = duration.ToString()
                        }

                    });
                }

            }
            else
            {
                resetTempsPasserelleArr(guid, arrivalFlight);
            }
        }

        private void resetTempsPasserelleArr(Guid guid, Flight arrivalFlight)
        {
            if (FlightMethods.getCFValueForFlight(arrivalFlight, Configuration.widgetParameters["ArrDurationPasserelle"].ToString()) != "0")
            {
                FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, arrivalFlight, new List<Sita.AMSClient.AMSSoapService.PropertyValue>
                    {
                        new Sita.AMSClient.AMSSoapService.PropertyValue
                        {
                            propertyNameField =Configuration.widgetParameters["ArrDurationPasserelle"].ToString(),
                            valueField = "0"
                        }

                    });
            }
        }

        private List<Sita.AMSClient.AMSSoapService.PropertyValue> translatePVFromMSMQtoSOAP(List<Sita.AMSClient.MSMQNotifications.PropertyValue> myPVToSend)
        {
            List<Sita.AMSClient.AMSSoapService.PropertyValue> toReturn = new List<Sita.AMSClient.AMSSoapService.PropertyValue>();


            foreach (var item in myPVToSend)
            {
                toReturn.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                {
                    propertyNameField = item.propertyName,
                    valueField = item.Value


                }) ;


            }

            return toReturn;
        }


        private bool isAstationnementDenuit(DateTime inBlockTime, DateTime offBlockTime)
        {

            int groundDuration = (int)(offBlockTime - inBlockTime).TotalMinutes;
            int minimumStationnement = Convert.ToInt32(Configuration.widgetParameters["DureeNightStationnement"]);

            int countingMinutes = 0;

            TimeSpan eveningTransitionTime = TimeSpan.FromHours(Convert.ToInt32(Configuration.widgetParameters["EndDayTime"].ToString().Substring(0, 2))) + TimeSpan.FromMinutes(Convert.ToInt32(Configuration.widgetParameters["EndDayTime"].ToString().Substring(2, 2)));
            TimeSpan morningTransitionTime = TimeSpan.FromHours(Convert.ToInt32(Configuration.widgetParameters["StartDayTime"].ToString().Substring(0, 2))) + TimeSpan.FromMinutes(Convert.ToInt32(Configuration.widgetParameters["StartDayTime"].ToString().Substring(2, 2)));

            for (DateTime zetime = inBlockTime.AddMinutes(1); zetime <= offBlockTime; zetime = zetime.AddMinutes(1))

            {
                TimeSpan zeTimeOfzeDay = zetime.TimeOfDay;
                if (zeTimeOfzeDay == eveningTransitionTime) countingMinutes = 0; // reset


                if (zeTimeOfzeDay > eveningTransitionTime || zeTimeOfzeDay <= morningTransitionTime) countingMinutes++; // adds a minute if we are between 2200 and 0600.

                if (countingMinutes >= minimumStationnement) return true; // bingo! exit

            }

            return false;

        }




        private bool thatFlightHasBlockTime(Flight myFlight)
        {
            string propertyToConsider = (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival) ? Configuration.widgetParameters["AIBT"].ToString() : Configuration.widgetParameters["AOBT"].ToString();

            string BlockTime = FlightMethods.getCFValueForFlight(myFlight, propertyToConsider);

            if (BlockTime == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

  


        /// <summary>
        /// This function will be called when the service is stopped, see the onStop method of service host
        /// </summary>
        /// <param name="TransactionId"></param>
        public void OnStopService(string TransactionId)
        {
            // Stop threads, release resources
            // Show message, do some treatements etc.....

            host.Close();

            LogManager.TraceLog(Level.Error, "CustomServiceManager", "OnStopService", TransactionId, "Widget is stopped");
        }
        #endregion
    }
}
