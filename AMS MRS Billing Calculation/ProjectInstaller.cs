﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.ServiceProcess;
using System.Threading.Tasks;

namespace AMS_MRS_Indisponibilités
{
    [RunInstaller(true)]
    public partial class ProjectInstaller : System.Configuration.Install.Installer
    {
        public ProjectInstaller()
        {
            InitializeComponent();
        }
        protected override void OnBeforeInstall(IDictionary savedState)
        {
            if (DoesServiceExist(serviceInstaller1.ServiceName))
            {
                StopServiceIfRunning(serviceInstaller1.ServiceName);
                deleteService(serviceInstaller1.ServiceName);
            }
            base.OnBeforeInstall(savedState);
        }
        private void deleteService(string serviceName)
        {
            ServiceInstaller ServiceInstallerObj = new ServiceInstaller();
            InstallContext Context = new InstallContext();
            ServiceInstallerObj.Context = Context;
            ServiceInstallerObj.ServiceName = serviceName;
            ServiceInstallerObj.Uninstall(null);
        }
        void StopServiceIfRunning(string serviceName)
        {
            ServiceController service = new ServiceController(serviceName);

            if ((service.Status.Equals(ServiceControllerStatus.Running)) ||

                (service.Status.Equals(ServiceControllerStatus.StartPending)))

            {
                TimeSpan timeout = TimeSpan.FromMilliseconds(10000);

                service.Stop();
                service.WaitForStatus(ServiceControllerStatus.Running, timeout);
            }
        }
        bool DoesServiceExist(string serviceName)
        {
            ServiceController[] services = ServiceController.GetServices();
            var service = services.FirstOrDefault(s => s.ServiceName == serviceName);
            return service != null;
        }
    }
}
