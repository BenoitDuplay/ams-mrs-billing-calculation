﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;
using AMS_MRS_Indisponibilités;
using Sita.AMSClient.AMSSoapService;
using Sita.AMSClient.MSMQNotifications;
using Sita.AMSClient.RestMSMQNotifications;
using Sita.Logger;
using Sita.Configuration;
using Sita.Toolkit;
using System.Net;

namespace AMS_MRS_Indisponibilités.helpers
{
    public partial class FlightMethods


    {
       
        public static void initStandDico(Dictionary<string, string> myStandDico)
        {
            Guid guid = Guid.NewGuid();
            HttpStatusCode result = CustomServiceManager.WidgetManager.AmsRestClient.Stands_GetAllAsync(CustomServiceManager.Configuration.widgetParameters["HomeAirport"],
               guid.ToString(), out object data, out string error);



            if (result == HttpStatusCode.OK)
            {

                ArrayOfFixedResource myStandCollection = (ArrayOfFixedResource)data;

                myStandDico.Clear();


                if (myStandCollection.FixedResource != null)
                {
                    foreach (FixedResource item in myStandCollection.FixedResource)
                    {

                        string categoryForBilling = "";
                        foreach (CustomField PV in item.CustomFields)
                        {
                            if (PV.Name == CustomServiceManager.Configuration.widgetParameters["TypologieParkingCF"].ToString())
                            {
                                categoryForBilling = PV.Value;
                                break;
                            }
                        }


                        myStandDico.Add(item.Name, categoryForBilling);
                    }
                }



                LogManager.TraceLog(Level.Info, "CustomServiceManager", "", "initStandDico", "Acquired " + myStandDico.Count.ToString() + " Stands");


            }
            else
            {

                LogManager.TraceLog(Level.Error, "CustomServiceManager", "", "initStandDico",  "Error getting stands list: "  + error);


            }





        }

        internal static void DoTheConsolidatedUpdateOnFlights(Guid guid)
        {
       

            foreach (Flight item in myRealTimeFlightsToPVUpdateDico.Keys)
            {
                if (myRealTimeFlightsToPVUpdateDico[item].Count != 0)
                {
                    UpdateTheFlightWithTheseValuesIfNecessary(guid, item, myRealTimeFlightsToPVUpdateDico[item], out bool thereHasBeenARealUpdate);
                }
            }
        }

      

        public static bool UpdateTheFlightWithTheseValuesIfNecessary(Guid guid, Flight myFlight, List<Sita.AMSClient.AMSSoapService.PropertyValue> myValuesToUpdate, out bool thereHasBeenARealUpdate)
        {
            // first get rid of duplicate PV names. this means that a Value has been calculated by the widget and we should not use the reset value.

            thereHasBeenARealUpdate = false;

            removeDuplicatePVs(myValuesToUpdate);
            
            //  check if the values need to be updated in a new list

            List<Sita.AMSClient.AMSSoapService.PropertyValue> myValuesToSendAsTheyAreNew = new List<Sita.AMSClient.AMSSoapService.PropertyValue>();

            foreach (var item in myValuesToUpdate)
            {
                if (getCFValueForFlight(myFlight, item.propertyNameField) != item.valueField)
                {
                    myValuesToSendAsTheyAreNew.Add(item);
                }
            }


            if (myValuesToSendAsTheyAreNew.Count != 0)
            {
                ResultStatus result = CustomServiceManager.WidgetManager.AmsSoapClient.UpdateFlight(guid.ToString(), translateIdFromMSMQtoSOAP(myFlight.FlightId), myValuesToSendAsTheyAreNew.ToArray(),
                                     out ErrorInformation[] error, out object data);

                if (error == null || error.Length == 0)
                {
                    thereHasBeenARealUpdate = true;
                    return true;
                }
                else
                {
                    LogManager.TraceLog(Level.Error, "CustomServiceManager" //className
                             , "updateTheFlightWithTheseValuesIfNecessary", guid.ToString(),
                             "Error Updating Flight: " + (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival ? "Arr" : "Dep")
                             + myFlight.FlightId.AirlineDesignator[0].Value + myFlight.FlightId.FlightNumber
                            + " " + myFlight.FlightId.ScheduledDate.ToString("ddMMMyy", CultureInfo.InvariantCulture)); ;


                    return false;
                }

            }


            return true;

        }

        private static void removeDuplicatePVs(List<Sita.AMSClient.AMSSoapService.PropertyValue> myValuesToUpdate)
        {
            Dictionary<string, List<Sita.AMSClient.AMSSoapService.PropertyValue>> keyValuePairs = new Dictionary<string, List<Sita.AMSClient.AMSSoapService.PropertyValue>>();
            // this dico will store all PV names and their corresponding objects
            foreach (var item in myValuesToUpdate)
            {
                if (!keyValuePairs.ContainsKey(item.propertyNameField)) keyValuePairs.Add(item.propertyNameField, new List<Sita.AMSClient.AMSSoapService.PropertyValue>());
                keyValuePairs[item.propertyNameField].Add(item);
            }

            // now gather  the occurances of duplicate except the last one. FIFO

            List<Sita.AMSClient.AMSSoapService.PropertyValue> propertyValuesToDelete = new List<Sita.AMSClient.AMSSoapService.PropertyValue>();


            foreach (var item in keyValuePairs)
            {
                if (item.Value.Count >1)

                    for (int i = 0; i < item.Value.Count-1; i++)
                    {
                        propertyValuesToDelete.Add(item.Value[i]);
                    }
            }

            // now delete from the orifinal list

            foreach (var item in propertyValuesToDelete)
            {
                myValuesToUpdate.Remove(item);
            }

        }

        public static Dictionary<Flight, List<Sita.AMSClient.AMSSoapService.PropertyValue>> myRealTimeFlightsToPVUpdateDico = new Dictionary<Flight, List<Sita.AMSClient.AMSSoapService.PropertyValue>>();


        public static void RecordFlightUpdatesForFutureConsolidation(Guid guid, Flight myFlight, List<Sita.AMSClient.AMSSoapService.PropertyValue> myValuesToUpdate)
        {
            RecordFlightUpdatesForFutureConsolidation(guid, myFlight, myValuesToUpdate, myRealTimeFlightsToPVUpdateDico); // add the default, which isreal time flight
        }

        public static void  RecordFlightUpdatesForFutureConsolidation(Guid guid, Flight myFlight, List<Sita.AMSClient.AMSSoapService.PropertyValue> myValuesToUpdate,
            Dictionary<Flight, List<Sita.AMSClient.AMSSoapService.PropertyValue>> myFlightsToPVUpdateDico)
        {
            if (!myFlightsToPVUpdateDico.ContainsKey(myFlight)) // ad to the dico if it does not exist
            {
                myFlightsToPVUpdateDico.Add(myFlight, new List<Sita.AMSClient.AMSSoapService.PropertyValue>());
            }

            myFlightsToPVUpdateDico[myFlight].AddRange(myValuesToUpdate);
        }


        public static string getCFValueForFlight(Flight myFlight, string MCField)
        {
            string toReturn = "";

            Sita.AMSClient.MSMQNotifications.PropertyValue[] CFields = myFlight.FlightState.Value;

            foreach (Sita.AMSClient.MSMQNotifications.PropertyValue PV in CFields)
            {
                if (PV.propertyName == MCField)
                {
                    toReturn = PV.Value;
                    break;
                }
            }


            return toReturn;
        }

        public static string getAttributeValueForMovement(Movement myMovement, string CFToFind)
        {

            foreach (Sita.AMSClient.MSMQNotifications.PropertyValue item in myMovement.MovementState.Value)
            {
                if (item.propertyName == CFToFind)
                {
                    return item.Value.ToString();
                }

            }

            return "";

        }


        public static Sita.AMSClient.AMSSoapService.FlightId translateIdFromMSMQtoSOAP(Sita.AMSClient.MSMQNotifications.FlightId myOriginalFlightId)
        {



            Sita.AMSClient.AMSSoapService.FlightId flightIDToReturn = new Sita.AMSClient.AMSSoapService.FlightId();

            flightIDToReturn.flightKindField = myOriginalFlightId.FlightKind ==
                Sita.AMSClient.MSMQNotifications.FlightKind.Arrival ? Sita.AMSClient.AMSSoapService.FlightKind.Arrival : Sita.AMSClient.AMSSoapService.FlightKind.Departure;
            flightIDToReturn.flightNumberField = myOriginalFlightId.FlightNumber;
            flightIDToReturn.scheduledDateField = myOriginalFlightId.ScheduledDate;

            Sita.AMSClient.AMSSoapService.LookupCode[] myPVArray1 = new Sita.AMSClient.AMSSoapService.LookupCode[1];
            Sita.AMSClient.AMSSoapService.LookupCode myPV1 = new Sita.AMSClient.AMSSoapService.LookupCode
            {
                codeContextField = Sita.AMSClient.AMSSoapService.CodeContext.IATA,
                valueField = myOriginalFlightId.AirportCode[0].Value
            };
            myPVArray1[0] = myPV1;




            flightIDToReturn.airportCodeField = myPVArray1;


            Sita.AMSClient.AMSSoapService.LookupCode[] myPVArray = new Sita.AMSClient.AMSSoapService.LookupCode[1];
            Sita.AMSClient.AMSSoapService.LookupCode myPV = new Sita.AMSClient.AMSSoapService.LookupCode
            {
                codeContextField = Sita.AMSClient.AMSSoapService.CodeContext.IATA,
                valueField = myOriginalFlightId.AirlineDesignator[0].Value
            };
            myPVArray[0] = myPV;

            flightIDToReturn.airlineDesignatorField = myPVArray;

            return flightIDToReturn;
        }


    }
}
