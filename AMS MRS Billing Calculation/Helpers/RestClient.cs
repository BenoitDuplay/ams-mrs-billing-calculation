﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Xml;

namespace AMS_MRS_Indisponibilités.helpers
{
    public enum HttpVerb
    {
        GET,
        PATCH,
        POST,
        PUT,
        DELETE
    }
    public  class RestClient
    {

        
            public string EndPoint { get; set; }
            public HttpVerb Method { get; set; }
            public string ContentType { get; set; }
            public string PostData { get; set; }
            public string token { get; set; }

            public RestClient()
            {
                EndPoint = "";
                Method = HttpVerb.GET;
                ContentType = "application/xml";
                PostData = "";
            }
            public RestClient(string endpoint)
            {
                EndPoint = endpoint;
                Method = HttpVerb.GET;
                ContentType = "application/xml";
                PostData = "";
            }
            public RestClient(string endpoint, HttpVerb method)
            {
                EndPoint = endpoint;
                Method = method;
                ContentType = "application/xml";
                PostData = "";
            }

            public RestClient(string endpoint, HttpVerb method, string tokenToUse)
            {
                EndPoint = endpoint;
                Method = method;
                ContentType = "application/xml";
                token = tokenToUse;
                PostData = "";
            }



            public RestClient(string endpoint, HttpVerb method, string postData, string tokenToUse)
            {
                EndPoint = endpoint;
                Method = method;
                ContentType = "application/xml";
                PostData = postData;
                token = tokenToUse;
            }

            public string MakeRequest()
            {

                HttpWebRequest request = (HttpWebRequest)WebRequest.Create(EndPoint);

                // string message = "step 2.1: created request";


                request.Method = Method.ToString();
                request.ContentLength = 0;
                request.ContentType = ContentType;
                request.Headers.Add("Authorization", token);


                if (!string.IsNullOrEmpty(PostData) && (Method == HttpVerb.PUT || Method == HttpVerb.POST || Method == HttpVerb.PATCH))
                {
                    var encoding = new UTF8Encoding();
                    var bytes = Encoding.GetEncoding("iso-8859-1").GetBytes(PostData);
                    request.ContentLength = bytes.Length;

                    using (var writeStream = request.GetRequestStream())
                    {
                        writeStream.Write(bytes, 0, bytes.Length);
                    }
                }

                //  message = "step 2.2: encoded request. Sending....";
                // MessageBox.Show(message);




                HttpWebResponse response = null;
                string responseValue = string.Empty;
                string result = string.Empty;

                try
                {
                    using (response = request.GetResponse() as HttpWebResponse)
                    {
                        if (request.HaveResponse && response != null)
                        {
                            using (var reader = new StreamReader(response.GetResponseStream()))
                            {
                                result = reader.ReadToEnd();
                            }
                        }




                        if (response.StatusCode != HttpStatusCode.OK)
                        {
                            // grab the response
                            using (var responseStream = response.GetResponseStream())
                            {
                                if (responseStream != null)
                                    using (var reader = new StreamReader(responseStream))
                                    {
                                        responseValue = reader.ReadToEnd();
                                    }
                            }

                            result = response.StatusCode + " value: " + responseValue;

                            return result;
                        }
                    }

                    /*
                    // grab the response
                    using (var responseStream = response.GetResponseStream())
                    {
                        if (responseStream != null)
                            using (var reader = new StreamReader(responseStream))
                            {
                                responseValue = reader.ReadToEnd();
                            }
                        else
                        {
                            responseValue = "OK";f
                        }
                    }*/





                }
                catch (WebException wex)
                {
                    if (wex.Message.Contains("nable to connect"))
                    {
                        result = "Error: " + wex.Message;

                    }



                    else
                    {
                        var drillResponse = ((HttpWebResponse)wex.Response);


                        if (drillResponse != null)
                        {
                            var content = drillResponse.GetResponseStream();
                            var reader = new StreamReader(content);


                            string tt = reader.ReadToEnd();
                            if (tt.Length == 0) return result;



                            XmlDocument myDoc = new XmlDocument();



                            try
                            {
                                myDoc.LoadXml(tt);

                                result = "Error: " + myDoc.InnerText;

                            }
                            catch (Exception)
                            {
                            }
                        }
                        else
                        {
                            result = "Error: Check Connectivity to REST API.";

                        }



                    }

                }

                return result;
            }

        }


    }
