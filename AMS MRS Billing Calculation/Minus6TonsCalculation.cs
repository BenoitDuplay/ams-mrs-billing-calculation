﻿using AMS_MRS_Indisponibilités.helpers;
using Sita.AMSClient.AMSSoapService;
using Sita.AMSClient.MSMQNotifications;
using Sita.Logger;
using Sita.Toolkit;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AMS_MRS_Indisponibilités
{
    class Minus6TonsCalculation
    {
        


        public static bool ExamineTheListForMinus6TBasé(Guid guid, Dictionary<string, List<Movement>> myDicoOfMinus6TMovementBaséPerImmat, Dictionary<Flight, List<Sita.AMSClient.AMSSoapService.PropertyValue>> myMinus6TFlightsToPVUpdateDico)
        {
            foreach (string immat in myDicoOfMinus6TMovementBaséPerImmat.Keys)
            {
                List<Movement> myListOfMovementOrderedByAIBT = myDicoOfMinus6TMovementBaséPerImmat[immat];
                myListOfMovementOrderedByAIBT = myListOfMovementOrderedByAIBT.OrderBy(x => FlightMethods.getCFValueForFlight(x.ArrivalFlight, CustomServiceManager.Configuration.widgetParameters["AIBT"].ToString())).ToList();

                DateTime myLastDayChecked = DateTime.MinValue;

                foreach (Movement myMovement in myListOfMovementOrderedByAIBT)
                {
                    DateTime myAIBT = DateTime.ParseExact(FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, CustomServiceManager.Configuration.widgetParameters["AIBT"].ToString()), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                    DateTime myAOBT = DateTime.ParseExact(FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, CustomServiceManager.Configuration.widgetParameters["AOBT"].ToString()), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                    int count = 0; // to be sure we reset to 0


                    List<StandSlot> standSlots = new List<StandSlot>(); // this will be the list that we will work on
                        standSlots.AddRange(myMovement.ArrivalFlight.FlightState.StandSlots);


                        List<StandSlot> slotsWithinFranchise = new List<StandSlot>();
                    if (myMovement.ArrivalFlight!= null && myMovement.ArrivalFlight.FlightId.FlightNumber == "381")
                    {

                    }

                        CustomServiceManager.ApplyTheFranchiseToTheseStandSlots(myAIBT, standSlots, out slotsWithinFranchise); // shave the list with the franchise, 

                         count = GetTheNumberOfDaysWhereNonAbriandNonPrivatifIsused(myAIBT, myAOBT, standSlots);


                    


                    DateTime myAIBTPlusFranchise = DateTime.MinValue; // we need to check from the AIBt + franchise to make sure we do not uncount below.

                    if (slotsWithinFranchise.Count >= 1) { 
                        myAIBTPlusFranchise = TimeZoneInfo.ConvertTimeFromUtc(
                            DateTime.ParseExact(CustomServiceManager.getPropertyOfSlot(slotsWithinFranchise.LastOrDefault(), "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture)
                            , CustomServiceManager.MRSTimeZone);

                    }
                    else // bug found 25jan21...
                    {
                        myAIBTPlusFranchise = TimeZoneInfo.ConvertTimeFromUtc(
                        myAIBT
                         , CustomServiceManager.MRSTimeZone);
                    }
                    
                    if (myLastDayChecked == myAIBTPlusFranchise.Date) count--; // does not do anything with the next immat during the same day as we have already computed what's necessary

                    if (count < 0) count = 0;
                        myLastDayChecked = TimeZoneInfo.ConvertTimeFromUtc(myAOBT, CustomServiceManager.MRSTimeZone).Date;
                        if (count == 0) myLastDayChecked = myAIBTPlusFranchise.Date.AddDays(-1); // if we do not count the first ones, we need to contonue working on the otehrs so that at least one gets a count

                    List<Sita.AMSClient.AMSSoapService.PropertyValue> PVToSend = new List<Sita.AMSClient.AMSSoapService.PropertyValue>();
                    PVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                    {
                        propertyNameField = CustomServiceManager.Configuration.widgetParameters["StatJourHorsAbris"].ToString(),
                        valueField = count.ToString()

                    });
                    PVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                    {
                        propertyNameField = CustomServiceManager.Configuration.widgetParameters["ForfaitStat"].ToString(),
                        valueField = "0"

                    }); // this would reset the forfait if a non basé becomes a basé...



                    FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, myMovement.ArrivalFlight, PVToSend, myMinus6TFlightsToPVUpdateDico);

                }

            }

            return true;
        }

      

        private static int GetTheNumberOfDaysWhereNonAbriandNonPrivatifIsused(DateTime myAIBT, DateTime myAOBT, List<StandSlot> standSlots)
        {
            int count = 0;

            for (DateTime zeDayInMRSTime = TimeZoneInfo.ConvertTimeFromUtc(myAIBT, CustomServiceManager.MRSTimeZone).Date; zeDayInMRSTime <= TimeZoneInfo.ConvertTimeFromUtc(myAOBT, CustomServiceManager.MRSTimeZone).Date; zeDayInMRSTime = zeDayInMRSTime.AddDays(1)) // check all the days,
            {
                if (ThereIsANonPrivatifSlotInThatDay(standSlots, zeDayInMRSTime) && ThereIsANonAbrisSlotInThatDay(standSlots, zeDayInMRSTime)) count++;

            }

            return count;
        }

        private static bool ThereIsANonAbrisSlotInThatDay(List<StandSlot> standSlots, DateTime zeDayINMRSTime)
        {
            foreach (var item in standSlots)
            {
                if (CustomServiceManager.theSlotIsInThatDay(item, zeDayINMRSTime) && CustomServiceManager.getCategoryOfStandInSlot(item) != "Abris")
                {
                    return true;
                }

            }

            return false;
        }

        private static  bool ThereIsANonPrivatifSlotInThatDay(List<StandSlot> standSlots, DateTime zeDayInMRSTime)
        {
            foreach (var item in standSlots)
            {
                if (CustomServiceManager.theSlotIsInThatDay(item, zeDayInMRSTime) && CustomServiceManager.getCategoryOfStandInSlot(item) != "Privatif")
                {
                    return true;
                }

            }

            return false;
        }

        public static void doTheDurationCalculationForLessThan6TonsNonBase(Guid guid, Movement myMovement, Dictionary<Flight, List<Sita.AMSClient.AMSSoapService.PropertyValue>> myMinus6TFlightsToPVUpdateDico)
        {
            DateTime myAIBT = DateTime.ParseExact(FlightMethods.getCFValueForFlight(myMovement.ArrivalFlight, CustomServiceManager.Configuration.widgetParameters["AIBT"].ToString()), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
            DateTime myAOBT = DateTime.ParseExact(FlightMethods.getCFValueForFlight(myMovement.DepartureFlight, CustomServiceManager.Configuration.widgetParameters["AOBT"].ToString()), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

            List<StandSlot> standSlots = new List<StandSlot>(); // this will be the list that we will work on
            standSlots.AddRange(myMovement.ArrivalFlight.FlightState.StandSlots);



            List<StandSlot> slotsWithinFranchise = new List<StandSlot>();


            CustomServiceManager.ApplyTheFranchiseToTheseStandSlots(myAIBT, standSlots, out slotsWithinFranchise); // shave the list with the franchise, useless here ??? 

            int count = 0;

            for (DateTime zeDayInMRSTime = TimeZoneInfo.ConvertTimeFromUtc(myAIBT, CustomServiceManager.MRSTimeZone).Date.AddDays(1); zeDayInMRSTime <= TimeZoneInfo.ConvertTimeFromUtc(myAOBT, CustomServiceManager.MRSTimeZone ).Date; zeDayInMRSTime = zeDayInMRSTime.AddDays(1)) // check all the days,
            {
                if (ThereIsANonAbrisSlotInThatDay(standSlots, zeDayInMRSTime)) count++;

            }



            // send the value to the update consolidation method

            List<Sita.AMSClient.AMSSoapService.PropertyValue> PVToSend = new List<Sita.AMSClient.AMSSoapService.PropertyValue>();
            PVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
            {
                propertyNameField = CustomServiceManager.Configuration.widgetParameters["StatJourHorsAbris"].ToString(),
                valueField = count.ToString()

            });
            PVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
            {
                propertyNameField = CustomServiceManager.Configuration.widgetParameters["ForfaitStat"].ToString(),
                valueField = "1"

            }); // this would reset the forfait if a non basé becomes a basé...


            FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, myMovement.ArrivalFlight, PVToSend, myMinus6TFlightsToPVUpdateDico);
        }


    }
}
