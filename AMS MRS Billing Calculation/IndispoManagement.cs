﻿using Sita.Configuration;
using Sita.Toolkit;
using Sita.Logger;
using System.Threading;
using System;
using System.Collections.Generic;
using Sita.AMSClient.RestMSMQNotifications;
using System.Linq;
using System.ComponentModel;
using System.Globalization;
using Sita.AMSClient.MSMQNotifications;
using Sita.AMSClient.AMSSoapService;
using AMS_MRS_Indisponibilités.helpers;

namespace AMS_MRS_Indisponibilités
{
   public class IndispoManagement

    {
        public static DateTime startTimeToCheckFlightsFor { get; private set; }
        public static DateTime endTimeToCheckFlightsFor { get; private set; }

        List<string> myListOfStandSWithPasserelle = new List<string>();
        private static int daysToAcquireintheFuture = 30;
        private static int daysToAcquireinthePast = -40;



        #region Methods
        public static void InitTheIndispoEnvironment()
        {
            // Read widget specefic configuration variables
            // Read the worker delay from the configuration file 

            try
            {
                daysToAcquireinthePast = Convert.ToInt32(CustomServiceManager.Configuration.widgetParameters["daysToAcquireinthePast"]);
                daysToAcquireintheFuture = 1;

            }
            catch (Exception)
            {
                LogManager.TraceLog(Level.Info, "IndispoManagement", "InitTheIndispoEnvironment", "", "Exception reading daysToAcquireSettings");

            }


            //getAndUpdateListOfStandsWithPasserelle();

         


            do
            {
                try
                {
                    initializeDowngradeDicos();
                }
                catch (Exception)
                {
                    LogManager.TraceLog(Level.Error, "IndispoManagement", "InitTheIndispoEnvironment", "", "Exception whilst updating the DG dico");

                }

                Thread.Sleep(new TimeSpan(24, 0, 0)); // reset the DG list every 24 hours.

            } while (true);

        }

        private static void initializeDowngradeDicos()
        {
            BackgroundWorker downGradeGAquisitionBGW = new BackgroundWorker();
            downGradeGAquisitionBGW.DoWork += DownGradeGAquisitionBGW_DoWork;
            downGradeGAquisitionBGW.RunWorkerCompleted += DownGradeGAquisitionBGW_RunWorkerCompleted;


            downGradeGAquisitionBGW.RunWorkerAsync();

        }


        public static void DownGradeGAquisitionBGW_RunWorkerCompleted(object sender, RunWorkerCompletedEventArgs e)
        {


            LogManager.TraceLog(Level.Info, "CustomServiceManager" //className
          , "DowngradeAcquisitionCompleted", "", "Acquired " + myDicoOfFilteredStandDG.Count.ToString() + " Downgrades from " + DateTime.Now.Date.AddDays(daysToAcquireinthePast).ToString("dd MMM", CultureInfo.InvariantCulture)
                 + " to " + DateTime.Now.Date.AddDays(daysToAcquireintheFuture).ToString("dd MMM.", CultureInfo.InvariantCulture));


            LogManager.TraceLog(Level.Info, "CustomServiceManager" //className
           , "DowngradeAcquisitionCompleted", "", "Now checking the flights");


            BackgroundWorker StartupFlightCheckAquisitionBGW = new BackgroundWorker();
            StartupFlightCheckAquisitionBGW.DoWork += StartupFlightCheckAquisitionBGW_DoWork;

            StartupFlightCheckAquisitionBGW.RunWorkerAsync();
        }

        private static void StartupFlightCheckAquisitionBGW_DoWork(object sender, DoWorkEventArgs e)
        {
            Guid guid = Guid.NewGuid();

            // day by day from today to the past, check the flights

            for (DateTime zeDay = DateTime.Now.Date.AddDays(daysToAcquireintheFuture); zeDay >= DateTime.Now.Date.AddDays(daysToAcquireinthePast); zeDay = zeDay.AddDays(-1))
            {

                LogManager.TraceLog(Level.Info, "CustomServiceManager" //className
      , "StartupFlightCheckAquisitionBGW_DoWork", "", "Checking for flights that need updating on " + zeDay.ToString("dd MMM.", CultureInfo.InvariantCulture));


                checkAllFlightsFor400HzOrPasserelleChangeInBetween(guid, zeDay, zeDay.AddDays(1), "slow");
                Thread.Sleep(1000);

            }
        }

        public static void checkAllFlightsFor400HzOrPasserelleChangeInBetween(Guid guid, DateTime myStartTime, DateTime myEndTime, string slow)
        {
            FlightCollection myFlights = getFlightsForThatWindow(guid, myStartTime, myEndTime);

            if (myFlights != null && myFlights.Flight != null)
            {

                foreach (Flight myflight in myFlights.Flight)
                {
                    string parameterToSeek = myflight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival ? "EtatFacturationCF_Arr":"EtatFacturationCF_Dep";

                    string Status = FlightMethods.getCFValueForFlight(myflight, CustomServiceManager.Configuration.widgetParameters[parameterToSeek].ToString());

                    // if all is ok only (block time + OK status) we do the update below
                    if (thatFlightHasBlockTime(myflight) &&
                        CustomServiceManager.Configuration.widgetParameters["EtatOKPourCalculForIndispoEtStatNuit"].Contains(Status))
                    {
                        if (slow == "slow") Thread.Sleep(500);

                        List<Sita.AMSClient.AMSSoapService.PropertyValue> myPVToSend = checkAndUpdateTheFlight400HzOrPasserelleIndispoCFIfNecessary(guid, myflight, true);

                        if (myPVToSend!= null && myPVToSend.Count >0)
                        {
                            updateTheFlightWithTheseValues(guid, myflight, myPVToSend);
                        }
                    }
                }
            }
        }

        private static void checkAllFlightsFor400HzOrPasserelleChangeInBetween(Guid guid, DateTime myStartTime, DateTime myEndTime)
        {

            checkAllFlightsFor400HzOrPasserelleChangeInBetween(guid, myStartTime, myEndTime, "");

        }


        public static void DownGradeGAquisitionBGW_DoWork(object sender, DoWorkEventArgs e)
        {

            Guid guid = Guid.NewGuid();


            for (DateTime zeDay = DateTime.Now.Date.AddDays(daysToAcquireinthePast); zeDay <= DateTime.Now.Date.AddDays(daysToAcquireintheFuture); zeDay = zeDay.AddDays(1))
            {

                StandDowngradeCollection mySDCollectionForTheDay = getStandCloisonDownGradesWithinWindow(guid, zeDay, zeDay.AddDays(1));
                // filter the ones we are interested in

                if (mySDCollectionForTheDay != null && mySDCollectionForTheDay.StandDowngrade != null)
                {
                    foreach (StandDowngradeInformation item in mySDCollectionForTheDay.StandDowngrade)
                    {

                        if (thisDGSatisfiedTheFilter(item)) addorSwapToTheDGDico(item);

                    }
                }


            }
        }


        private static StandDowngradeCollection getStandCloisonDownGradesWithinWindow(Guid guid, DateTime start, DateTime end)
        {


            ResultStatus result = CustomServiceManager. WidgetManager.AmsSoapClient.GetStandDowngrades(guid.ToString(), start, end,
                CustomServiceManager.Configuration.widgetParameters["HomeAirport"],
                Sita.AMSClient.AMSSoapService.AirportIdentifierType.IATACode, out ErrorInformation[] error, out object data);


            if (error == null || error.Length == 0)
            {

                return data as StandDowngradeCollection;
            }
            else
            {
                return null;
            }

        }


        public static Dictionary<string, StandDowngradeInformation> myDicoOfFilteredStandDG = new Dictionary<string, StandDowngradeInformation>();

        public static void addorSwapToTheDGDico(StandDowngradeInformation item)
        {
            string id = item.DowngradeIdentifier.DowngradeUniqueId;
            if (myDicoOfFilteredStandDG.ContainsKey(id))
            {
                myDicoOfFilteredStandDG[id] = item;
            }
            else
            {
                myDicoOfFilteredStandDG.Add(id, item);
            }

        }

        /// <summary>
        /// Returns a boolean that is true if the downgrade in question has a reason that is meaningful
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static bool thisDGSatisfiedTheFilter(StandDowngradeInformation item)
        {
            string reason = getPropertyInStandDowngrade(item, "Reason");

            string r1 = CustomServiceManager.Configuration.widgetParameters["ReasonIndispo400HzPosteA"].Replace(" ", string.Empty);
            string r2 = CustomServiceManager.Configuration.widgetParameters["ReasonIndispo400HzPosteC"].Replace(" ", string.Empty);
            string r3 = CustomServiceManager.Configuration.widgetParameters["ReasonsIndispoPass_Sud"].Replace(" ", string.Empty);
            string r4 = CustomServiceManager.Configuration.widgetParameters["ReasonsIndispoPass_Nord"].Replace(" ", string.Empty);


            reason = reason.Replace(" ", string.Empty);
            if (reason == r1 ||
                reason == r2
              || r3.Contains(reason) || r4.Contains(reason)
               )
                return true;
            else return false;
        }


        private  static string getPropertyInStandDowngrade(StandDowngradeInformation item, string PropToFind)
        {
            foreach (Sita.AMSClient.MSMQNotifications.PropertyValue PV in item.StandDowngradeState.Value)
            {
                if (PV.propertyName == PropToFind)
                {
                    if (PV.Value == null) return "";

                    return PV.Value;
                }
            }
            return "";
        }




        /// <summary>
        /// The task that will treat the message
        /// </summary>
        /// <param name="message"></param>
        /// <param name="queueName"></param>
        /// <param name="serverName"></param>
        /// <param name="returnedObject"></param>
        /// <param name="transactionId"></param>


        public static void TreatStandDowngradeDeletedNotification(Guid guid, StandDowngradeDeletedNotification standDowngradeDeletedNotification)
        {

            LogManager.TraceLog(Level.Info, "CustomServiceManager", "TreatStandDowngradeUpdatedNotification", guid.ToString(),"Deletion spotted.");


            if (myDicoOfFilteredStandDG.ContainsKey(standDowngradeDeletedNotification.DowngradeIdentifier.DowngradeUniqueId))
            {
                startTimeToCheckFlightsFor = DateTime.ParseExact(getPropertyInStandDowngrade(myDicoOfFilteredStandDG[standDowngradeDeletedNotification.DowngradeIdentifier.DowngradeUniqueId], "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                endTimeToCheckFlightsFor = DateTime.ParseExact(getPropertyInStandDowngrade(myDicoOfFilteredStandDG[standDowngradeDeletedNotification.DowngradeIdentifier.DowngradeUniqueId], "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                removeFromDGDico(standDowngradeDeletedNotification.DowngradeIdentifier.DowngradeUniqueId);

                if (endTimeToCheckFlightsFor > DateTime.Now.AddDays(daysToAcquireinthePast) && startTimeToCheckFlightsFor < DateTime.Now.AddHours(1)) // only if it is in the window
                {

                    checkAllFlightsFor400HzOrPasserelleChangeInBetween(guid, startTimeToCheckFlightsFor, endTimeToCheckFlightsFor);
                }
            }
        }


        public static void TreatStandDowngradeUpdatedNotification(Guid guid, StandDowngradeUpdatedNotification standDowngradeUpdatedNotification)
        {
            // clean up old Dopwgrades

            List<string> standDowngradesListToDelete = new List<string>();

            foreach (var item in myDicoOfFilteredStandDG)
            {
                DateTime now = DateTime.Now;

                StandDowngradeInformation standDowngradeInformation = item.Value;
                DateTime endTimeToCheckFor = DateTime.ParseExact(getPropertyInStandDowngrade(standDowngradeInformation, "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                if (endTimeToCheckFor < now.AddDays(daysToAcquireinthePast))
                {
                    standDowngradesListToDelete.Add(item.Key);
                }
            }

            foreach (var item in standDowngradesListToDelete)
            {
                myDicoOfFilteredStandDG.Remove(item);
            }

            // then do the job

            LogManager.TraceLog(Level.Info, "IndispoManagement", "TreatStandDowngradeUpdatedNotification", guid.ToString(), "Update spotted.");

            if (!thisRelatesTo400HzOrPasserelle(standDowngradeUpdatedNotification.StandDowngrade))
            {

                LogManager.TraceLog(Level.Info, "IndispoManagement", "TreatStandDowngradeUpdatedNotification", guid.ToString(), "Nothing to do with resources that are being watched.");
                return;
            }

            if (myDicoOfFilteredStandDG.ContainsKey(standDowngradeUpdatedNotification.StandDowngrade.DowngradeIdentifier.DowngradeUniqueId))
            {
                // what was in memory
                DateTime myPreviousStartTime = DateTime.ParseExact(getPropertyInStandDowngrade(myDicoOfFilteredStandDG[standDowngradeUpdatedNotification.StandDowngrade.DowngradeIdentifier.DowngradeUniqueId], "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                DateTime myPreviousEndTime = DateTime.ParseExact(getPropertyInStandDowngrade(myDicoOfFilteredStandDG[standDowngradeUpdatedNotification.StandDowngrade.DowngradeIdentifier.DowngradeUniqueId], "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                // what is now
                startTimeToCheckFlightsFor = DateTime.ParseExact(getPropertyInStandDowngrade(standDowngradeUpdatedNotification.StandDowngrade, "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                endTimeToCheckFlightsFor = DateTime.ParseExact(getPropertyInStandDowngrade(standDowngradeUpdatedNotification.StandDowngrade, "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                if (startTimeToCheckFlightsFor > myPreviousStartTime) startTimeToCheckFlightsFor = myPreviousStartTime;
                if (endTimeToCheckFlightsFor < myPreviousEndTime) endTimeToCheckFlightsFor = myPreviousEndTime;

                doTheDicoWorkAndCheckTheFlightsFor(guid, startTimeToCheckFlightsFor, endTimeToCheckFlightsFor, standDowngradeUpdatedNotification);

            }
            else // it needs to be added to the list!
            {
                startTimeToCheckFlightsFor = DateTime.ParseExact(getPropertyInStandDowngrade(standDowngradeUpdatedNotification.StandDowngrade, "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                endTimeToCheckFlightsFor = DateTime.ParseExact(getPropertyInStandDowngrade(standDowngradeUpdatedNotification.StandDowngrade, "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                doTheDicoWorkAndCheckTheFlightsFor(guid, startTimeToCheckFlightsFor, endTimeToCheckFlightsFor, standDowngradeUpdatedNotification);

            }

        }



        private static void doTheDicoWorkAndCheckTheFlightsFor(Guid guid, DateTime startTimeToCheckFlightsFor, DateTime endTimeToCheckFlightsFor, StandDowngradeUpdatedNotification myNotif)
        {

            addorSwapToTheDGDico(myNotif.StandDowngrade);

            if (endTimeToCheckFlightsFor > DateTime.Now.AddDays(daysToAcquireinthePast) && startTimeToCheckFlightsFor < DateTime.Now.AddHours(daysToAcquireintheFuture*24)) // only if it is in the window
            {

                checkAllFlightsFor400HzOrPasserelleChangeInBetween(guid, startTimeToCheckFlightsFor, endTimeToCheckFlightsFor);

            }
            else
            {
                LogManager.TraceLog(Level.Info, "IndispoManagement", "doTheDicoWorkAndCheckTheFlightsFor", guid.ToString(), "received DG outside of the window. No checks on flights necessary");

            }
        }



        public static void TreatStandDowngradeCreatedNotification(Guid guid, StandDowngradeCreatedNotification standDowngradeCreatedNotification)
        {
            if (!thisRelatesTo400HzOrPasserelle(standDowngradeCreatedNotification.StandDowngrade))
            {
                LogManager.TraceLog(Level.Info, "IndispoManagement", "TreatStandDowngradeCreatedNotification", guid.ToString(), "Nothing to do with resources that are being watched.");
                return;
            }

            startTimeToCheckFlightsFor = DateTime.ParseExact(getPropertyInStandDowngrade(standDowngradeCreatedNotification.StandDowngrade, "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
            endTimeToCheckFlightsFor = DateTime.ParseExact(getPropertyInStandDowngrade(standDowngradeCreatedNotification.StandDowngrade, "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

            addorSwapToTheDGDico(standDowngradeCreatedNotification.StandDowngrade);
            if (endTimeToCheckFlightsFor > DateTime.Now.AddDays(daysToAcquireinthePast) && startTimeToCheckFlightsFor < DateTime.Now.AddHours(1)) ////// only if it is in the window
            {
                checkAllFlightsFor400HzOrPasserelleChangeInBetween(guid, startTimeToCheckFlightsFor, endTimeToCheckFlightsFor);

            }
            else
            {
                LogManager.TraceLog(Level.Info, "IndispoManagement", "TreatStandDowngradeCreatedNotification", guid.ToString(), "received DG creation outside of the window");
            }
        }




        private static void removeFromDGDico(string downgradeUniqueId)
        {
            if (myDicoOfFilteredStandDG.ContainsKey(downgradeUniqueId)) myDicoOfFilteredStandDG.Remove(downgradeUniqueId);

        }


        private static bool thisRelatesTo400HzOrPasserelle(StandDowngradeInformation mySDInfo) // returns true if the downgrade corresponds to somethin to watch for. For ex. Cloison downgrades will be disregarded
        {
            List<string> allAcceptableReasons = new List<string>{ CustomServiceManager.Configuration.widgetParameters["ReasonIndispo400HzPosteA"],
                CustomServiceManager.Configuration.widgetParameters["ReasonIndispo400HzPosteC"]};



            foreach (var item in CustomServiceManager.Configuration.widgetParameters["ReasonsIndispoPass_Nord"].Split(',').ToList())
            {
                allAcceptableReasons.Add(item);
            }
            foreach (var item in CustomServiceManager.Configuration.widgetParameters["ReasonsIndispoPass_Sud"].Split(',').ToList())
            {
                allAcceptableReasons.Add(item);
            }


            if (allAcceptableReasons.Contains(getPropertyInStandDowngrade(mySDInfo, "Reason"))) return true; // it is straight in the vale


            if (mySDInfo.StandDowngradeChange != null && mySDInfo.StandDowngradeChange.Change != null) // check if there has been a change of reason that corresponds to one of the settings
            {

                foreach (Sita.AMSClient.MSMQNotifications.PropertyChange myPC in mySDInfo.StandDowngradeChange.Change)
                {

                    if (myPC.propertyName == "Reason" &
                        (allAcceptableReasons.Contains(myPC.NewValue) || allAcceptableReasons.Contains(myPC.OldValue)))
                    {
                        return true;
                    }
                }
            }




            return false;
        }




        public static void TreatMovementUpdatedNotificationForIndispo(Guid guid, Movement myMovement)
        {
            List<Sita.AMSClient.AMSSoapService.PropertyValue> myPVToSendForArrival = new List<Sita.AMSClient.AMSSoapService.PropertyValue>();
            List<Sita.AMSClient.AMSSoapService.PropertyValue> myPVToSendForDeparture = new List<Sita.AMSClient.AMSSoapService.PropertyValue>();


            if (myMovement.ArrivalFlight != null && thatFlightHasBlockTime(myMovement.ArrivalFlight)) ///////
            {
                myPVToSendForArrival = checkAndUpdateTheFlight400HzOrPasserelleIndispoCFIfNecessary(guid, myMovement.ArrivalFlight, false);
                FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, myMovement.ArrivalFlight, myPVToSendForArrival);
                Console.WriteLine("Arrivals: " + myPVToSendForArrival.Count.ToString());
            }
            else
            {

                LogManager.TraceLog(Level.Info, "IndispoManagement", "TreatFlightUpdatedNotification", guid.ToString(), "The Arrival Flight has no block time. Do nothing for Indispo");
            }

            if (myMovement.DepartureFlight != null && thatFlightHasBlockTime(myMovement.DepartureFlight)) ///////
            {

                myPVToSendForDeparture = checkAndUpdateTheFlight400HzOrPasserelleIndispoCFIfNecessary(guid, myMovement.DepartureFlight, false);
                FlightMethods.RecordFlightUpdatesForFutureConsolidation(guid, myMovement.DepartureFlight, myPVToSendForDeparture);
                Console.WriteLine("Departures: " + myPVToSendForDeparture.Count.ToString());
            }
            else
            {

                LogManager.TraceLog(Level.Info, "IndispoManagement", "TreatFlightUpdatedNotification", guid.ToString(), "The Departure Flight has no block time. Do nothing for Indispo");
            }

        }

        private static List<Sita.AMSClient.AMSSoapService.PropertyValue> checkAndUpdateTheFlight400HzOrPasserelleIndispoCFIfNecessary(Guid guid, Flight myFlight, bool doNotCheckForSlotChangeAsTheFlightComesFromaGetFlightAfterADowngradeChange)
        {
            string myIndispoStatusUpdateFor400HzPosteA = "Non";
            string myIndispoStatusUpdateFor400HzPosteC = "Non";

            string myIndispoStatusUpdateForPasserelleNord = "Non";
            string myIndispoStatusUpdateForPasserelleSud = "Non";

            List<Sita.AMSClient.AMSSoapService.PropertyValue> myPVToSend = new List<Sita.AMSClient.AMSSoapService.PropertyValue>();


            // checks if there are slots changes

            if ((myFlight.FlightChanges != null && myFlight.FlightChanges.StandSlotsChange != null)|| thereHasBeenAChangeInFacturationStatus(myFlight) || doNotCheckForSlotChangeAsTheFlightComesFromaGetFlightAfterADowngradeChange)
            {
                if (myFlight.FlightState.StandSlots != null && myFlight.FlightState.StandSlots.Length != 0) // there is at least a slot
                {
                    string arrivalStand = "";
                    string departureStand = "";

                    if (myFlight.FlightState.StandSlots[0].Stand != null)
                    {
                        arrivalStand = myFlight.FlightState.StandSlots[0].Stand.Value[0].Value;
                    }


                    if (myFlight.FlightState.StandSlots[myFlight.FlightState.StandSlots.Length - 1].Stand != null)
                    {
                        departureStand = myFlight.FlightState.StandSlots[myFlight.FlightState.StandSlots.Length - 1].Stand.Value[0].Value;
                    }



                    DateTime InBlockTime = DateTime.ParseExact(getPropertyInSlot(typeof(StandSlot), myFlight.FlightState.StandSlots[0], "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                    DateTime OffBlockTime = DateTime.ParseExact(getPropertyInSlot(typeof(StandSlot), myFlight.FlightState.StandSlots[myFlight.FlightState.StandSlots.Length - 1], "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);


                    List<StandDowngradeInformation> myListOfCurrentSD = getStandDowngradesActiveAtThatTime(myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival ? InBlockTime : OffBlockTime);

                    foreach (StandDowngradeInformation item in myListOfCurrentSD)

                    {
                        // need to check if it is on the correct resource ..

                        bool matchInAtLeastAResource = false; // will store yes if there is a match

                        foreach (Sita.AMSClient.MSMQNotifications.StandResource standResource in item.StandDowngradeState.Stands)
                        {
                            string toCompare = standResource.Value[0].Value;
                            if (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival && toCompare == arrivalStand
                                || myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Departure && toCompare == departureStand)
                            {
                                matchInAtLeastAResource = true;
                                break;
                            }
                        }

                        if (matchInAtLeastAResource)
                        {

                            if (getPropertyInStandDowngrade(item, "Reason") == CustomServiceManager.Configuration.widgetParameters["ReasonIndispo400HzPosteA"])
                            {
                                myIndispoStatusUpdateFor400HzPosteA = "Oui";

                            }
                            else if (getPropertyInStandDowngrade(item, "Reason") == CustomServiceManager.Configuration.widgetParameters["ReasonIndispo400HzPosteC"])
                            {
                                myIndispoStatusUpdateFor400HzPosteC = "Oui";

                            }

                            if (CustomServiceManager.Configuration.widgetParameters["ReasonsIndispoPass_Nord"].Contains(getPropertyInStandDowngrade(item, "Reason")))
                            {
                                myIndispoStatusUpdateForPasserelleNord = "Oui";

                            }
                            else if (CustomServiceManager.Configuration.widgetParameters["ReasonsIndispoPass_Sud"].Contains(getPropertyInStandDowngrade(item, "Reason")))
                            {
                                myIndispoStatusUpdateForPasserelleSud = "Oui";
                            }
                        }
                    }
                }
               
           
                // below check for poste A

                //string myCurrentValueToCompareWith = (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival) ?
                //    getAttributeValueForFlight(myFlight, CustomServiceManager.Configuration.widgetParameters["IndispoPosteAArrCF"]) :
                //    getAttributeValueForFlight(myFlight, CustomServiceManager.Configuration.widgetParameters["IndispoPosteADepCF"]);


                //if (myIndispoStatusUpdateFor400HzPosteA != myCurrentValueToCompareWith)
                //{
                    myPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                    {
                        propertyNameField = (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival)?
                        CustomServiceManager.Configuration.widgetParameters["IndispoPosteAArrCF"] : CustomServiceManager.Configuration.widgetParameters["IndispoPosteADepCF"],
                        valueField = myIndispoStatusUpdateFor400HzPosteA
                    });
                //}

                //myCurrentValueToCompareWith = (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival) ?
                //   getAttributeValueForFlight(myFlight, CustomServiceManager.Configuration.widgetParameters["IndispoPosteCArrCF"]) :
                //   getAttributeValueForFlight(myFlight, CustomServiceManager.Configuration.widgetParameters["IndispoPosteCDepCF"]);

                // then below check for poste C
                //if (myIndispoStatusUpdateFor400HzPosteC != myCurrentValueToCompareWith)
                //{
                    myPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                    {
                        propertyNameField = (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival) ?
                        CustomServiceManager.Configuration.widgetParameters["IndispoPosteCArrCF"] : CustomServiceManager.Configuration.widgetParameters["IndispoPosteCDepCF"],
                        valueField = myIndispoStatusUpdateFor400HzPosteC
                    });
                //}




                // below check for Passerelle Nord

                //myCurrentValueToCompareWith = (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival) ?
                //   getAttributeValueForFlight(myFlight, CustomServiceManager.Configuration.widgetParameters["IndispoPasserelleNordArrCF"]) :
                //   getAttributeValueForFlight(myFlight, CustomServiceManager.Configuration.widgetParameters["IndispoPasserelleNordDepCF"]);


                //if (myIndispoStatusUpdateForPasserelleNord != myCurrentValueToCompareWith)
                //{
                    myPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                    {
                        propertyNameField = (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival) ?
                        CustomServiceManager.Configuration.widgetParameters["IndispoPasserelleNordArrCF"] : CustomServiceManager.Configuration.widgetParameters["IndispoPasserelleNordDepCF"],
                        valueField = myIndispoStatusUpdateForPasserelleNord
                    });
                //}

                //myCurrentValueToCompareWith = (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival) ?
                //   getAttributeValueForFlight(myFlight, CustomServiceManager.Configuration.widgetParameters["IndispoPasserelleSudArrCF"]) :
                //   getAttributeValueForFlight(myFlight, CustomServiceManager.Configuration.widgetParameters["IndispoPasserelleSudDepCF"]);

                //// then below check for Passerelle Sud
                //if (myIndispoStatusUpdateForPasserelleSud != myCurrentValueToCompareWith)
                //{
                    myPVToSend.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                    {
                        propertyNameField = (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival) ?
                        CustomServiceManager.Configuration.widgetParameters["IndispoPasserelleSudArrCF"] : CustomServiceManager.Configuration.widgetParameters["IndispoPasserelleSudDepCF"],
                        valueField = myIndispoStatusUpdateForPasserelleSud
                    });
                //}


            }
            return myPVToSend;
        }

        private static bool thereHasBeenAChangeInFacturationStatus(Flight myFlight)
        {

            string CF = (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival) ?
               CustomServiceManager.Configuration.widgetParameters["EtatFacturationCF_Arr"] :
               CustomServiceManager.Configuration.widgetParameters["EtatFacturationCF_Dep"];

            if (myFlight.FlightChanges!= null && myFlight.FlightChanges.Change != null)
            {
                foreach (Sita.AMSClient.MSMQNotifications.PropertyChange changeInformation in myFlight.FlightChanges.Change)
                {
                    if (changeInformation.propertyName == CF)
                    {
                        return true;
                    }
                }
            }

          
            return false;
        }

        private static List<Sita.AMSClient.AMSSoapService.PropertyValue> translatePVFromMSMQtoSOAP(List<Sita.AMSClient.MSMQNotifications.PropertyValue> myPVToSend)
        {
            List<Sita.AMSClient.AMSSoapService.PropertyValue> toReturn = new List<Sita.AMSClient.AMSSoapService.PropertyValue>();


            foreach (var item in myPVToSend)
            {
                toReturn.Add(new Sita.AMSClient.AMSSoapService.PropertyValue
                {
                    propertyNameField = item.propertyName,
                    valueField = item.Value


                });


            }

            return toReturn;
        }

        private static string getAttributeValueForFlight(Flight myFlight, string MCField)
        {
            string toReturn = "";

            Sita.AMSClient.MSMQNotifications.PropertyValue[] CFields = myFlight.FlightState.Value;

            foreach (Sita.AMSClient.MSMQNotifications.PropertyValue PV in CFields)
            {
                if (PV.propertyName == MCField)
                {
                    toReturn = PV.Value;
                    break;
                }
            }

            return toReturn;
        }



        private static List<StandDowngradeInformation> getStandDowngradesActiveAtThatTime(DateTime inBlockTime)
        {
            List<StandDowngradeInformation> myListToreturn = new List<StandDowngradeInformation>();

            foreach (StandDowngradeInformation myStandDowngradeInformation in myDicoOfFilteredStandDG.Values)
            {

                DateTime myStartTime = DateTime.MinValue;
                DateTime myEndTime = DateTime.MinValue;


                myStartTime = DateTime.ParseExact(getPropertyInStandDowngrade(myStandDowngradeInformation, "StartTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);
                myEndTime = DateTime.ParseExact(getPropertyInStandDowngrade(myStandDowngradeInformation, "EndTime"), "yyyy-MM-ddTHH:mm:00", CultureInfo.InvariantCulture);

                if (myStartTime <= inBlockTime && inBlockTime <= myEndTime)
                {
                    myListToreturn.Add(myStandDowngradeInformation);
                }

            }

            return myListToreturn;

        }

        private static bool updateTheFlightWithTheseValues(Guid guid, Flight myFlight, List<Sita.AMSClient.AMSSoapService.PropertyValue> myValuesToUpdate)
        {

            ResultStatus result = CustomServiceManager.WidgetManager.AmsSoapClient.UpdateFlight(guid.ToString(), translateIdFromMSMQtoSOAP(myFlight.FlightId), myValuesToUpdate.ToArray(),
                                        out ErrorInformation[] error, out object data);


            if (error == null || error.Length == 0)
            {
                return true;
            }
            else
            {
                return false;
            }

        }

        private static Sita.AMSClient.AMSSoapService.FlightId translateIdFromMSMQtoSOAP(Sita.AMSClient.MSMQNotifications.FlightId myOriginalFlightId)
        {



            Sita.AMSClient.AMSSoapService.FlightId flightIDToReturn = new Sita.AMSClient.AMSSoapService.FlightId();

            flightIDToReturn.flightKindField = myOriginalFlightId.FlightKind ==
                Sita.AMSClient.MSMQNotifications.FlightKind.Arrival ? Sita.AMSClient.AMSSoapService.FlightKind.Arrival : Sita.AMSClient.AMSSoapService.FlightKind.Departure;
            flightIDToReturn.flightNumberField = myOriginalFlightId.FlightNumber;
            flightIDToReturn.scheduledDateField = myOriginalFlightId.ScheduledDate;

            Sita.AMSClient.AMSSoapService.LookupCode[] myPVArray1 = new Sita.AMSClient.AMSSoapService.LookupCode[1];
            Sita.AMSClient.AMSSoapService.LookupCode myPV1 = new Sita.AMSClient.AMSSoapService.LookupCode { codeContextField = Sita.AMSClient.AMSSoapService.CodeContext.IATA, valueField = myOriginalFlightId.AirportCode[0].Value };
            myPVArray1[0] = myPV1;




            flightIDToReturn.airportCodeField = myPVArray1;


            Sita.AMSClient.AMSSoapService.LookupCode[] myPVArray = new Sita.AMSClient.AMSSoapService.LookupCode[1];
            Sita.AMSClient.AMSSoapService.LookupCode myPV = new Sita.AMSClient.AMSSoapService.LookupCode { codeContextField = Sita.AMSClient.AMSSoapService.CodeContext.IATA, valueField = myOriginalFlightId.AirlineDesignator[0].Value };
            myPVArray[0] = myPV;

            flightIDToReturn.airlineDesignatorField = myPVArray;

            return flightIDToReturn;
        }

        private string getAttributeFromFixedResource(FixedResource fixedResource, string CF)
        {
            foreach (CustomField item in fixedResource.CustomFields)
            {
                if (item.Name == CF)
                {
                    return item.Value;
                }
            }
            return "";
        }

        private static string getPropertyInSlot(Type myClass, object mySlot, string PropToFind)
        {
            Sita.AMSClient.MSMQNotifications.PropertyValue[] myPVs = null;

            if (myClass == typeof(StandSlot))
            {
                myPVs = ((StandSlot)mySlot).Value;
            }
            if (myClass == typeof(GateSlot))
            {
                myPVs = ((GateSlot)mySlot).Value;
            }

            foreach (Sita.AMSClient.MSMQNotifications.PropertyValue PV in myPVs)
            {
                if (PV.propertyName == PropToFind)
                {
                    if (PV.Value == null) return "";

                    return PV.Value;
                }
            }

            return "";
        }






        private static bool thatFlightHasBlockTime(Flight myFlight)
        {
            if (myFlight == null) return false;
          

            string propertyToConsider = (myFlight.FlightId.FlightKind == Sita.AMSClient.MSMQNotifications.FlightKind.Arrival) ? CustomServiceManager.Configuration.widgetParameters["AIBT"] : CustomServiceManager.Configuration.widgetParameters["AOBT"];

            string BlockTime = getAttributeValueForFlight(myFlight, propertyToConsider);

            if (BlockTime == "")
            {
                return false;
            }
            else
            {
                return true;
            }
        }

        private static FlightCollection getFlightsForThatWindow(Guid guid, DateTime start, DateTime end)
        {
            try
            {
                //get the flights

                ResultStatus result = CustomServiceManager.WidgetManager.AmsSoapClient.GetFlights(guid.ToString(), start, end, CustomServiceManager.Configuration.widgetParameters["HomeAirport"],
                                         AirportIdentifierType.IATACode, out ErrorInformation[] error, out object data);


                LogManager.TraceLog(Level.Info, "IndispoManagement", "getFlightsForThatWindow", guid.ToString(), "Done Getting the flights");

                return data as FlightCollection;

            }
            catch (Exception)
            {
                LogManager.TraceLog(Level.Error, "IndispoManagement", "getFlightsForThatWindow", guid.ToString(), "Error Getting the flights");

            }



            return null;
        }

        #endregion
    }
}
